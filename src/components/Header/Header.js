import React from 'react';
// import '../../config/bootstrap/bootstrap.css';
// // import '../../config/bootstrap/bootstrap-responsive.css';
// // import '../../config/bootstrap/bootstrap-overrides.css'
// import '../../config/elements.css';
// import '../../config/layout.css';

import './Header.scss';
import Logo from './Components/Logo';
import Account from './Components/Account';
import Setting from './Components/Setting';
import Signin from './Components/Signin';
import styled from 'styled-components';
import Flex from '../Flex';
import SideBarToggleButton from '../SideBar/SideBarToggleButton';

const Wrapper = styled(Flex)`
  height: 48px;
  justify-content: flex-end;
  align-items: center;
  flex-direction: row;
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  background-color: #2a353f;
  padding: 0 1rem;
  z-index: 900;
`

const SideBarButton = styled.button`
  // display: none;
  padding: 0;
  width: 30px;
  margin-left: 20px;
  border: none;
`

const Header = props => (

// class Header extends React.Component {
//   constructor() {
//     super();
//   }

//   render() {
//     return (
      <Wrapper>
        <SideBarToggleButton />
        {/* <SideBarButton src={Hamburger} alt="menu" /> */}
        <Logo />
        <Flex>
          <Setting />
          <Signin />
        </Flex>
      </Wrapper>
    );

export default Header;