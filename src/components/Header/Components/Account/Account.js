import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const Wrapper = styled(Link)`
  display: flex;
  border-right: 1px solid #101417;
  border-left: 1px solid #101417;
  height: 48px;
  align-items: center;

  &:hover{
    background-color:rgba(25, 31, 36,0.6);
    text-decoration: none;
  }
`

const StyledAccount = styled.a`
  text-decoration: none;
  color: rgb(214, 214, 214);
  margin: ${(props)=>`${props.theme.gutter.xs} ${props.theme.gutter.md}`};
  font-size: 12px;

  &:hover{
    color: #ffffff;
    text-decoration: none;
  }
`

const Account =()=>(
  <Wrapper to="/users">
    <StyledAccount>Your account</StyledAccount>
  </Wrapper>
)

export default Account;