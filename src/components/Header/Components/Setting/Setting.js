import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Image from '../../../../images/btn-setting.png';

const Wrapper = styled(Link)`
  border-right: 1px solid #101417;
  height: 48px;
  display: flex;
  align-items: center;

  &:hover{
    background-color:rgba(25, 31, 36,0.6);
  }
`

const StyledSetting = styled.img`
  cursor: pointer;
  width: 18px;
  margin: ${(props)=>`${props.theme.gutter.xs} ${props.theme.gutter.md}`};
`

const Setting =()=>(
  <Wrapper to="/settings">
    <StyledSetting src={Image} alt="Settings" />
  </Wrapper>
)

export default Setting;