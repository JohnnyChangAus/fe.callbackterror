import React from 'react';
import styled from 'styled-components';
import Image from '../../../../images/share.svg';
import {Link} from "react-router-dom";

const Wrapper = styled(Link)`
  display: flex;
  height: 48px;
  align-items: center;
  padding-left: 8px;
  padding-right: 8px;
  margin-right: 0px;

  &:hover{
    background-color:rgba(25, 31, 36,0.6);
  }
`

const StyledSignin = styled.img`
  cursor: pointer;
  width: 18px;
  margin: ${(props)=>`${props.theme.gutter.xs} ${props.theme.gutter.xs}`};
`

const Signin =()=>(
  <Wrapper to="/logout">
    <StyledSignin src={Image} alt="Log out" />
  </Wrapper>
)

export default Signin;