import React from 'react';
import styled from 'styled-components';
import Image from '../../../../images/logo.png';
import {Link} from "react-router-dom";

const Wrapper = styled(Link)`
  display: flex;
  flex: 1;
`

const StyledLogo = styled.img`
  cursor: pointer;
  width: 135px;
  margin: ${(props)=>`${props.theme.gutter.xs} ${props.theme.gutter.md}`};
`

const Logo =()=>(
  <Wrapper to="/">
    <StyledLogo src={Image} alt="Aussie PT Logo"/>
  </Wrapper>
)

export default Logo;