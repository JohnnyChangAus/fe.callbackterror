import React from 'react';
import styled from 'styled-components';
import Flex from '../../../Flex';

const Wrapper = styled(Flex)`
  
` 

const StyledInput = styled.input`
  border: 1px solid red;
  width: 150px;
`

const SearchBar =()=>(
  <Wrapper>
    <StyledInput />
  </Wrapper>
)

export default SearchBar;