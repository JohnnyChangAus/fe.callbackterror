import React, { useState } from 'react'
import Axios from 'axios';

import '../../config/bootstrap/bootstrap.css'
import '../../config/bootstrap/bootstrap-responsive.css'
import '../../config/bootstrap/bootstrap-overrides.css'
import '../../config/elements.css'
import '../../config/icons.css'
import '../../config/layout.css'
import './StudentList.scss'
import ContactImage from '../../images/contact-img.png'
import ContactImage2 from '../../images/contact-img2.png'
import { AllStudent } from '../../api/StudentAPI'

const api = 'http://localhost:8000/students'

class StudentList extends React.Component {
    constructor() {
        super()
        this.state = {
            isLoading: true,
            data: {},
            path: '/studentdetails/'
        }
    }

    async componentDidMount() {
        const response = await AllStudent();
        this.setState({
            data: response.data,
            isLoading: false
        })
    }

    render() {
        if (this.state.isLoading) {
            return (<p>Loading...</p>)
        }
        return (
            <>
                <div className="content">
                    <div className="container-fluid">
                        <div id="pad-wrapper" className="users-list">
                            <div className="row-fluid header">
                                <h3>Student</h3>
                                <div className="span10 pull-right">
                                    {/* <input type="text" className="span5 search" placeholder="Type a user's name..." /> */}
                                    {/* <div className="ui-dropdown">
                                        <div className="head" data-toggle="tooltip" title="Click me!">
                                            Filter users
                                            <i className="arrow-down"></i>
                                        </div>
                                        <div className="dialog">
                                            <div className="pointer">
                                                <div className="arrow"></div>
                                                <div className="arrow_border"></div>
                                            </div>
                                            <div className="body">
                                                <p className="title">Show users where:</p>
                                                <div className="form">
                                                    <select>
                                                        <option />Prefer Name
                                                        <option />First Name
                                                        <option />Last Name
                                                        <option />Course
                                                        <option />Mobile
                                                </select>
                                                    <input type="text" />
                                                    <a href="/" className="btn-flat small">Add filter</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> */}
                                    <a href="/studentnew" className="btn-flat success pull-right">
                                        NEW STUDENT
                                    </a>
                                </div>
                            </div>
                            <div className="row-fluid table">
                                <table className="table table-hover">
                                    <thead>
                                        <tr>
                                            <th className="span4 sortable">
                                                Name
                                            </th>
                                            <th className="span3 sortable">
                                                <span className="line"></span>Course
                                            </th>
                                            <th className="span2 sortable">
                                                <span className="line"></span>Mobile
                                            </th>
                                            <th className="span3 sortable align-right">
                                                <span className="line"></span>Email
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.data.map((d) => (
                                            <tr className="first">
                                                <td>
                                                    <img src={ContactImage} alt="" className="img-circle avatar hidden-phone" />
                                                    <a href = {this.state.path+d._id} className="name">{d.prefer_name}</a>
                                                </td>
                                                <td>{d.course}</td>
                                                <td>{d.mobile}</td>
                                                <td className="align-right">
                                                    <a href="mailto:{d.email}">{d.email}</a>
                                                </td>
                                            </tr>
                                        ))}
                                        {/* ============================== */}
                                        {/* <tr>
                                            <td>
                                                <img src={ContactImage2} alt="" className="img-circle avatar hidden-phone" />
                                                <a href="/studentdetails" className="name">Helen</a>
                                            </td>
                                            <td>
                                                Zumba
                                        </td>
                                            <td>
                                                0498-765-432
                                        </td>
                                            <td className="align-right">
                                                <a href="/">alejandra@canvas.com</a>
                                            </td>
                                        </tr> */}
                                    </tbody>
                                </table>
                            </div>
                            {/* <div className="pagination pull-right">
                                <ul>
                                    <li><a href="/">&#8249;</a></li>
                                    <li><a className="active" href="/">1</a></li>
                                    <li><a href="/">2</a></li>
                                    <li><a href="/">3</a></li>
                                    <li><a href="/">4</a></li>
                                    <li><a href="/">5</a></li>
                                    <li><a href="/">&#8250;</a></li>
                                </ul>
                            </div> */}
                        </div>
                    </div>
                </div>

            </>
        )
    }
}




export default StudentList