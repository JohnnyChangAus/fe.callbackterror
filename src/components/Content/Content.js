import React from 'react';
import styled from 'styled-components';
import Flex from '../Flex';

const Wrapper = styled(Flex)`
  margin-left: 180px;
  margin-top: 48px;
  width: 100%;
  min-height: 600px;
`

const Content =() =>(
  <Wrapper>
    <h1>This is dashboard which will display at least 10 upcoming courses with relevant students</h1>
  </Wrapper>
)

export default Content;