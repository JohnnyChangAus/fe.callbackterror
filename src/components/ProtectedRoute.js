import React, {useState, useEffect} from 'react'
import { Redirect, Route } from "react-router";
import { getStorage } from "../service/auth";
import { validateRoot } from "../api/UserAPI"

const ProtectedRoute = ({ component: Component, ...rest }) => {

    const [status, setStatus] = useState({
        status: 0,
        isLoading: true,
    })

    useEffect(() => {
        async function getStatus () {
            try {
                const res = await validateRoot(getStorage())
                console.log(res.status)
                setStatus({
                    status: res.status,
                    isLoading: false,
                })
            } catch (err) {
                console.log(err.response.status)
                setStatus({
                    status: err.response.status,
                    isLoading: false,
                })
            }
        }
        getStatus()
    }, [])

    if(status.isLoading) {
        return <div>Loading...</div>
    }
    
    return <Route {...rest} render={(props) => {
        console.log(props)
        console.log(status.status === 401)
        if (status.status === 200) {
            return <Component {...props} />
        } else {
            return <Redirect to="/" />
        }
    }} />


}

export default ProtectedRoute