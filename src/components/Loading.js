import React from 'react'
import styled from 'styled-components';
import { FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { faSync } from '@fortawesome/free-solid-svg-icons'

const Backdrop = styled.section`
position: fixed;
display: flex;
width: 100%;
height: 100%;
justify-content: center;
align-items: center;
background: rgba(0,0,0,0.5);
z-index: 100;
`

const Loading = () => (
    <Backdrop>
        <FontAwesomeIcon className='fa-spin fa-3x' icon={faSync} alt='loading...' />
    </Backdrop>
)

export default Loading