import React from 'react';
import '../../config/lib/font-awesome.css';
import '../../config/icons.css';


class SideBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clickMenu: false,
            sideBarOpen: false,
            clickStudent: false,
            clickCourse: false,
        }

    }



    clickStudent = (e) => {
        e.preventDefault();
        if (this.state.clickStudent === false) {
            this.setState({ clickStudent: true, clickCourse: false });
        } else {
            this.setState({ clickStudent: false });
        }
    }

    clickCourse = (e) => {
        e.preventDefault();
        if (this.state.clickCourse === false) {
            this.setState({ clickCourse: true, clickStudent: false });
        } else {
            this.setState({ clickCourse: false });
        }
    }

    clickMenu = () => {
        if (this.state.clickMenu) {
            this.setState({ clickMenu: false })
        } else this.setState({ clickMenu: true });
    }

    // sideBarToggleClickHandler = () => {
    //     if (this.state.sideBarOpen === false) {
    //         this.setState({ sideBarOpen: true })
    // }

    render() {
        return <>
            <div className="sidebar" >
                <div id="sidebar-nav" className={this.props.status} ref={this.props.innerRef}>
                    <ul id="dashboard-menu" >
                        <li>
                            {/* <div className="pointer">
                                <div className="arrow"></div>
                                <div className="arrow_border"></div>
                            </div> */}
                            <a href="/home">
                                <i className="icon-home"></i>
                                <span>Home</span>
                            </a>
                        </li>
                        <li className={this.state.clickStudent ? "active" : ""}>
                            {this.state.clickStudent ? <div className="pointer"><div className="arrow"></div>
                                <div className="arrow_border"></div></div> : ""}
                            <a className="dropdown-toggle" href="#" onClick={this.clickStudent}>
                                <i className="icon-group"></i>
                                <span>Students</span>
                                <i className="icon-chevron-down"></i>
                            </a>
                            <ul className={this.state.clickStudent ? "submenu active" : "submenu"}>
                                <li><a href="/studentlist">Student list</a></li>
                                <li><a href="/studentnew">New Student</a></li>
                            </ul>
                        </li>
                        <li className={this.state.clickCourse ? "active" : ""}>
                            {this.state.clickCourse ? <div className="pointer"><div className="arrow"></div>
                                <div className="arrow_border"></div></div> : ""}
                            <a className="dropdown-toggle" href="#" onClick={this.clickCourse}>
                                <i className="icon-edit"></i>
                                <span>Courses</span>
                                <i className="icon-chevron-down"></i>
                            </a>
                            <ul className={this.state.clickCourse ? "submenu active" : "submenu"}>
                                <li><a href="/courselist?page=1&pagesize=5">Course list</a></li>
                                <li><a href="/coursenew">New Course</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/generator">
                                <i className="icon-th-large"></i>
                                <span>Page creator</span>
                            </a>
                        </li>
                        <li>
                            <a href="/settings">
                                <i className="icon-cog"></i>
                                <span>Settings</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </>
    }
}
export default SideBar