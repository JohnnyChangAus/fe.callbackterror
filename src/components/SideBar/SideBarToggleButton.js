import React, {createRef} from 'react';
import SideBar from './SideBar';
import './SideBarToggleButton.scss'

class sideBarToggleButton extends React.Component {
    constructor () {
        super()
        this.menuRef = createRef();
        this.buttonRef = createRef();
        this.state = {
            clickMenu: false,
            
        }
    }

    onClickOutside = (e) => {

        const menuButton = document.querySelector(".toggle-button")
        const menuButtonLine = document.querySelectorAll(".toggle-button__line")

        if (this.menuRef.current.contains(e.target)) {
            this.setState({
                
                clickMenu: true,
            })
        } else if (menuButton === e.target  || Array.from(menuButtonLine).filter(buttonLine => buttonLine === e.target).length >= 1) {
            this.setState({
                
                clickMenu: !this.state.clickMenu,
            }) 
        } else {
            this.setState({
                
                clickMenu: false,
            })
        }
    }


    componentDidMount = () => {
        document.addEventListener('click', this.onClickOutside)

    }

    componentWillUnmount() {
        document.removeEventListener('click', this.onClickOutside)
      }

    render() {
        //console.log(this.state.clickOutside)
        return <>
    <button className="toggle-button" >
        <div className="toggle-button__line" />
        <div className="toggle-button__line" />
        <div className="toggle-button__line" />
    </button>
    
    {this.state.clickMenu   ? <SideBar status="display" innerRef={this.menuRef} /> : <SideBar innerRef={this.menuRef}/>}
    </>
    }
}

export default sideBarToggleButton;