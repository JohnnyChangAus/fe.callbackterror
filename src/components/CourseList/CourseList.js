import React from "react";
import { Link} from "react-router-dom";

import "./CourseList.scss";
import "../../config/bootstrap/bootstrap.css";
import "../../config/bootstrap/bootstrap-responsive.css";
import "../../config/bootstrap/bootstrap-overrides.css";
import "../../config/layout.css";
import "../../config/elements.css";
import "../../config/icons.css";
import "../../config/compiled/tables.css";


import * as CourseAPI from "../../api/CourseAPI";
import setCourse from '../../store/course/action/setCourse';
import { connect } from 'react-redux';

class CourseList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      page: 1,
      pages: [],
      pageSize: "10",
      data: {},
      currentPage: "1",
      keyword: "",
      inputValue: "",
      
    };
  }

  dataUpdate = async () => {
    const queryString = this.props.location.search;
    // console.log(queryString==="")

    if(queryString === "") {
      this.props.history.push('/home')
    } else {
      const queryArray = queryString.match(/([^=&?]+)=([^&]+)/g);
      let [page, pageSize, keyword] = queryArray.map(
        (query) => query.split("=")[1]
      );
  
  
      if (keyword === undefined) keyword = "";
      if (keyword.includes("%20")) keyword = keyword.replace("%20", " ")
  
      console.log(keyword)
  
      const response = await CourseAPI.allCourse(page, pageSize, keyword);

      //console.log(response)
      //console.log(response[0]._id)
      const pages = [...Array(Math.ceil(response.total / pageSize)).keys()];
      this.setState({
        data: response.results,
        page: page,
        pageSize: pageSize,
        pages: pages,
        isLoading: false,
        currentPage: page,
        keyword: keyword,
      });
    }
  }

  componentDidMount = () => {
    this.dataUpdate()
  };

  componentDidUpdate = (prevProps, prevState) => {
    // const { page: prevPage } = prevProps.match.params;
    // const { page: currentPage } = this.props.match.params;
    if (prevProps.location.search !== this.props.location.search) {
      this.dataUpdate()
    }
    // if (prevState.keyword !== this.state.keyword) {
    //     window.location.reload()
    // }
  };

  handleSearch = (e) => {

    this.setState({
      keyword: e.target.value,
    }, () => {
      this.props.history.push(
        `/courselist?page=1&pagesize=${this.state.pageSize}&keyword=${this.state.keyword}`
      );
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    // console.log("diao");

    this.props.history.push(
      `/courselist?page=1&pagesize=${this.state.pageSize}&keyword=${this.state.keyword}`
    );
  };

  handleDelete = async (e) => {
    e.preventDefault();
    // console.log(e.target.id)
    await CourseAPI.courseDelete(e.target.id)
    const response = await CourseAPI.allCourse(this.state.page, this.state.pageSize, this.state.keyword)
    const pages = [...Array(Math.ceil(response.total / 5)).keys()];
    this.setState({
      data: response.results,
      pages: pages,
    });
    
  }

  handleEdit = async (e) => {
    e.preventDefault();
    console.log(e.target.id)
    this.props.history.push(
      `/courseedit/${e.target.id}`
    );
  }

  handleAdd = async (e) => {
    e.preventDefault();
    console.log(e.target.id)
    this.props.history.push(
      `/coursenew`
    );
  }

  render() {

    const { showDetails } =this.props;
    
    if (this.state.isLoading) {
      return <p>Loading...</p>;
    }

    console.log(this.state.data);

    return (
      <div className="content">
        <div className="container-fluid">
          <div id="pad-wrapper">

            <div className="table-wrapper products-table section">
              {/* <div className="row-fluid head">
                <div className="span12">
                  <h3>Courses</h3>
                </div>
              </div> */}
              <div className="row-fluid filter-block">
              <input
                      className="search"
                      name="inputValue"
                      value={this.state.keyword}
                      onChange={this.handleSearch}
                    />
                <div className="pull-right">
                  {/* <p>{this.state.keyword}</p> */}
                  <form action="#" onSubmit={this.handleSubmit}>
                    

                    <a
                      className="btn-flat success new-product"
                      href="/"
                      onClick={this.handleAdd}
                    >
                      + Add course
                    </a>
                  </form>

                  {/* <button onClick={this.handleClick}>Search</button> */}
                  {/* <div className="ui-select">
                                        <select>
                                            <option>Filter users</option>
                                            <option>Signed last 30 days</option>
                                            <option>Active users</option>
                                        </select>
                                    </div> */}
                  {/* <input type="text" className="search" /> */}
                </div>
              </div>
              <p>{this.state.dataDeleted}</p>
              <div className="row-fluid">
                <table className="table table-hover">
                  
                  <thead>
                    <tr>
                      <th className="span2">Course Name</th>
                      <th className="span4">
                        <span className="line"></span>Course Target
                      </th>
                      {/* <th className="span1">
                                                <span className="line"></span>Active Students
                                            </th> */}
                      <th className="span2">
                        <span className="line"></span>
                        <span>Actions</span>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.data.map((d) => (
                      // <p>{d.course_name}</p>
                      <tr className="first">
                        <td>
                          {/* <div className="img">
                                                        <img src={TableImage} />
                                                    </div> */}
                          <a href="/coursedetails" id={d._id} onClick={()=>showDetails({
                              course_name: d.course_name,
                              duration: d.duration,
                              aims: d.aims,
                              description: d.description,
                              price: d.price,
                              id: d._id,
                          })} className="name">
                            {d.course_name}
                          </a>
                        </td>
                        <td className="description">{d.aims}</td>
                        {/* <td className="student_number">2</td> */}
                        <td>
                          {/* <span className="label label-success">{d.type}</span> */}
                          <ul className="actions">
                            <li>
                              <a href="/coursedetails" id={d._id} onClick={this.handleEdit}>Edit</a>
                            </li>
                            <li className="last">
                              <a href="/" id={d._id} onClick={this.handleDelete}>Delete</a>
                            </li>
                          </ul>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>

                <ul className="pages">
                  {this.state.pages.map((page, index) => (
                    <li
                      className={
                        `${page + 1}` === this.state.currentPage && "highlight"
                      }
                    >
                      <Link
                        to={`/courselist?page=${page + 1}&pagesize=${
                          this.state.pageSize
                        }&keyword=${this.state.keyword}`}
                        key={index}
                      >
                        {page + 1}
                      </Link>
                    </li>
                  ))}
                  
                </ul>
                
              </div>
              
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// export default CourseList;

const mapDispatchToProps = (dispatch) =>({
  showDetails: (course) => dispatch(setCourse(course)),
});

const CourseListContainer = connect(null,mapDispatchToProps)(CourseList);

export default CourseListContainer;