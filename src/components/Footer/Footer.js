import React from 'react';
import './Footer.scss';
import team_logo from '../../images/callbackterror.png';

const Footer = () => {
    return ( <>
        <h1 className="footer__title">This is Footer component</h1>
        <h2 className="footer__subtitle">Footer conetnt</h2>
        <div className="footer__image">
            <img src={team_logo} alt="team_logo"></img>
        </div>
    </>
    );
}

export default Footer;