import React from 'react'
import { clearStorage } from '../service/auth'
import { useEffect } from 'react'

const Logout = (props) => {
    useEffect(() => {
        clearStorage()
        const { history } = props
        history.push('/')
    })
    return(<></>)
}

export default Logout