import React from 'react';

import '../../config/bootstrap/bootstrap.css';
import '../../config/bootstrap/bootstrap-responsive.css';
import '../../config/bootstrap/bootstrap-overrides.css'
import '../../config/elements.css';
import '../../config/layout.css';

class PageGenerator extends React.Component {
    constructor() {
        super();
    };

    render() {
        return (
            <div className="content">
                <div className="container-fluid">
                    <div id="pad-wrapper">
                        <div className="table-wrapper products-table section">
                            <div className="row-fluid head">
                                <div className="span12">
                                    <h3 className="personal-info">Personal Web Page Creator</h3>
                                </div>
                            </div>
                            {/* <div class="alert alert-info">
                                <i class="icon-lightbulb"></i>
                                <p>This page shows an example of an alternative layout with the main navbar on the top
                                <br /> using <strong>.content.wide-content</strong> for this main container</p>
                            </div> */}

                            <div className="row-fluid filter-block">
                                <div className="">
                                    <div style={{display: "inline-flex"}} class="span9 alert alert-info">
                                        <i class="icon-lightbulb"></i>
                                        <p>Select the items below that you want to display on you personal web page</p>
                                    </div>
                                    {/* <input type="text" className="pull-left" /> */}
                                    <button className="btn-flat success pull-right">Preview page</button>
                                </div>
                            </div>
                            {/* table start */}
                            <div className="row-fluid">
                                <table className="table table-hover">
                                    <thead>
                                        <tr>
                                            <th className="span3">
                                                <input type="checkbox" />
                                                Iitmes
                                            </th>
                                            <th className="span6">
                                                <span className="line"></span>Details
                                            </th>
                                            {/* <th className="span3">
                                                <span className="line"></span>Status
                                            </th> */}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {/* <!-- row --> */}
                                        <tr className="first">
                                            <td className="span3 td-item">
                                                <input type="checkbox" />
                                                {/* <label>First name </label> */}
                                                <a href="#" className="name">First name</a>
                                            </td>
                                            <td className="td-description">
                                                Alejandra
                                            </td>
                                        </tr>
                                        {/* <!-- row --> */}
                                        <tr>
                                            <td>
                                                <input type="checkbox" />
                                                <a href="#" className="name">Last name</a>
                                            </td>
                                            <td className="description">
                                                Galvan
                                            </td>
                                        </tr>
                                        {/* <!-- row --> */}
                                        <tr>
                                            <td>
                                                <input type="checkbox" />
                                                <a href="#" className="name">Mobile</a>
                                            </td>
                                            <td className="description">
                                                0412-345-678
                                            </td>
                                        </tr>
                                        {/* <!-- row --> */}
                                        <tr>
                                            <td>
                                                <input type="checkbox" />
                                                <a href="#" className="name">Email</a>
                                            </td>
                                            <td className="description">
                                                alejandra@aussiept.com
                                            </td>
                                        </tr>
                                        {/* <!-- row --> */}
                                        <tr>
                                            <td>
                                                <input type="checkbox" />
                                                <a href="#" className="name">Location</a>
                                            </td>
                                            <td className="description">
                                                Brisbane
                                            </td>
                                        </tr>
                                        {/* <!-- row --> */}
                                        <tr>
                                            <td>
                                                <input type="checkbox" />
                                                <a href="#" className="name">About me</a>
                                            </td>
                                            <td className="description">
                                                I'm a personal trainer in greater Brisbane area.
                                            </td>
                                        </tr>
                                        {/* <!-- row --> */}
                                        <tr>
                                            <td>
                                                <input type="checkbox" />
                                                <a href="#" className="name">Instagram</a>
                                            </td>
                                            <td className="description">
                                                www.instagram.com
                                            </td>
                                        </tr>
                                        {/* <!-- row --> */}
                                        <tr>
                                            <td>
                                                <input type="checkbox" />
                                                <a href="#" className="name">Youtube</a>
                                            </td>
                                            <td className="description">
                                                www.youtube.com
                                            </td>
                                        </tr>
                                        {/* <!-- row --> */}
                                        <tr>
                                            <td>
                                                <input type="checkbox" />
                                                <a href="#" className="name">Course name</a>
                                            </td>
                                            <td className="description">
                                                Beginner's Workout Programme
                                            </td>
                                        </tr>
                                        {/* <!-- row --> */}
                                        <tr>
                                            <td>
                                                <input type="checkbox" />
                                                <a href="#" className="name">Duration</a>
                                            </td>
                                            <td className="description">
                                                4 weeks
                                            </td>
                                        </tr>
                                        {/* <!-- row --> */}
                                        <tr>
                                            <td>
                                                <input type="checkbox" />
                                                <a href="#" className="name">Aims</a>
                                            </td>
                                            <td className="description">
                                                Whether you're just starting out―or starting again―this fast-track workout plan will help you drastically improve your physique and fitness levels.
                                            </td>
                                        </tr>
                                        {/* <!-- row --> */}
                                        <tr>
                                            <td>
                                                <input type="checkbox" />
                                                <a href="#" className="name">Description</a>
                                            </td>
                                            <td className="description">
                                                This program isn’t just for the true beginner who has never touched a weight before; it’s also suitable for anyone who has taken an extended leave of absence from training. How long has it been since you went to the gym regularly? Six months? A year? Five years? No worries: The following routines will get you back on track in—you guessed it—just four short weeks. Let’s get to work.
                                            </td>
                                        </tr>
                                        {/* <!-- row --> */}
                                        <tr>
                                            <td>
                                                <input type="checkbox" />
                                                <a href="#" className="name">Price</a>
                                            </td>
                                            <td className="description">
                                                $400.00 aud
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="alert alert-info">
                                    <i class="icon-lightbulb"></i>
                                    <p>Out development team also provide customize professional web page service with bargain costs. Contact us if interested </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

};

export default PageGenerator