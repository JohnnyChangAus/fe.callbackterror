import React from 'react';

import '../../config/bootstrap/bootstrap.css';
import '../../config/bootstrap/bootstrap-responsive.css';
import '../../config/bootstrap/bootstrap-overrides.css'
import '../../config/elements.css';
import "../../config/icons.css";
import '../../config/layout.css';
import '../../config/compiled/personal-info.css'
import avatar from '../../images/personal-info.png';


class Settings extends React.Component {
    constructor() {
        super();
        this.state = {
        }
    }

    render() {
        return (
            <div className="content">
                <div className="container-fluid">
                    <div className="settings-wrapper" id="pad-wrapper">
                        <div className="span3 avatar-box">
                            <div className="personal-image">
                                <img src={avatar} className="avatar img-circle" alt="avatar" />
                                <p>Upload a different photo...</p>

                                <input type="file" name="" avatar />
                            </div>
                        </div>
                        <div className="span7 personal-info">
                            <h5 className="personal-title">Settings</h5>
                            <form>
                                <div className="field-box">
                                    <label>First name:</label>
                                    <input className="span5 inline-input" type="text" value="Alejandra" />
                                </div>
                                <div className="field-box">
                                    <label>Last name:</label>
                                    <input className="span5 inline-input" type="text" value="Galvan" />
                                </div>
                                <div className="field-box">
                                    <label>mobile:</label>
                                    <input className="span5 inline-input" type="text" value="0412-345-678" />
                                </div>
                                <div className="field-box">
                                    <label>Email:</label>
                                    <input className="span5 inline-input" type="text" value="alejandra@aussiept.com" />
                                </div>
                                <div className="field-box">
                                    <label>address:</label>
                                    <input className="span5 inline-input" type="text" value="" />
                                </div>
                                <div className="field-box">
                                    <label>about_me:</label>
                                    <input className="span5 inline-input" type="text" value="" />
                                </div>
                                <div className="field-box">
                                    <label>Instragram:</label>
                                    <input className="span5 inline-input" type="text" value="www.instagram.com" />
                                </div>
                                <div className="field-box">
                                    <label>youtube:</label>
                                    <input className="span5 inline-input" type="text" value="www.youtube.com" />
                                </div>
                                <div className="field-box">
                                    <label>password:</label>
                                    <input className="span5 inline-input" type="password" value="blablablabla" />
                                </div>
                                <div className="field-box">
                                    <label>Confirm password:</label>
                                    <input className="span5 inline-input" type="password" value="blablablabla" />
                                </div>
                                <div className="span6 field-box actions">
                                    <input type="button" className="btn-glow primary" value="Save Changes" />
                                    <span>OR</span>
                                    <input type="reset" value="Cancel" className="reset" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    };
};

export default Settings;