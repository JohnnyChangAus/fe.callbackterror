import React from "react";

class FormUnit extends React.Component {
  constructor(props){
    super(props)
    
  }
//  inputChange = (e) =>{
    
//   }
  render(){
    return (
      <div className='container-fluid setting-page__setting-form__unit '>
       <label htmlFor={this.props.htmlfor}>{this.props.lableText}:</label>
       <input
         type={this.props.inputType}
         name={this.props.inputName}
         placeholder= {`${this.props.promptMes}`}
         value={this.props.data}
         onChange= {(e)=> this.props.statuschange(e.target.name,e.target.value)}
       />
       {/* onfocus="this.placeholder=''"  onblur="this.placeholder=''" */}
       </div>
   );
  }
};
export default FormUnit
