import React from 'react'
import PropTypes from 'react'

import avartar from '../../images/personal-info.png';
import FormUnit from './settingFormUnit'



class SettingSection extends React.Component {
    constructor() {
        super()

        this.state = {
            firstName: '',
            password: '',
            email: '',
            mobile: '',
            address: '',
            aboutMe: '',
            instagram: '',
            youtube: ''


        }
    }

    handleChange = (DataName, Data) => {
        this.setState({
            [DataName]: Data
        })
    };
    render() {
        let firstName = this.state.firstName
        let password = this.state.password
        let email = this.state.email
        let mobile = this.state.mobile
        let address = this.state.address
        let aboutMe = this.state.aboutMe
        let instagram = this.state.instagram
        let youtube = this.state.youtube
        return (
            <div className="content">
                <div className='setting-page container-fluid'>
                    {JSON.stringify(this.state)}
                    <form action="">
                        <div className='row row-no-gutters'>
                            <div className='setting-page__avatar-area col-sm-3'>
                                <img src={avartar} alt="" />
                                <p>Upload a different photo...</p>
                                <input type="file" name='avatar' />

                            </div>
                            <div className='setting-page__setting-form col-sm-7'>
                                <h5>
                                    Personal info
                    </h5>
                                <FormUnit data={firstName} statuschange={this.handleChange} htmlfor='firstName' inputType='text' lableText='First Name' inputName='firstName' promptMes='Please enter your First Name' />
                                <FormUnit data={password} statuschange={this.handleChange} htmlfor='password' inputType='password' lableText='Password' inputName='password' promptMes='Please enter your Password' />
                                <FormUnit data={email} statuschange={this.handleChange} htmlfor='email' inputType='email' lableText='Email' inputName='email' promptMes='Please enter your Email' />
                                <FormUnit data={mobile} statuschange={this.handleChange} htmlfor='mobile' inputType='tel' lableText='Mobile' inputName='mobile' promptMes='Please enter your Mobile Number ' />
                                <FormUnit data={address} statuschange={this.handleChange} htmlfor='address' inputType='text' lableText='Adress' inputName='address' promptMes='Please enter your Adress' />
                                <div className='setting-page__setting-form__aboutMe '>
                                    <label htmlFor='aboutMe'>About Me</label>
                                    <textarea data={aboutMe} statuschange={this.handleChange} rows="8" name='aboutMe'></textarea>
                                </div>

                                <FormUnit data={instagram} statuschange={this.handleChange} htmlfor='instagram' inputType='text' lableText='Instagram' inputName='instagram' promptMes='' />
                                <FormUnit data={youtube} statuschange={this.handleChange} htmlfor='youtube' inputType='text' lableText='Youtube' inputName='youtube' promptMes='' />
                                <div className='setting-page__setting-form__submit-area'>
                                    <input type="submit" className='btn btn-primary setting-page__setting-form__submit-area__submit' value='Save Changes' />
                                    <span>OR</span>
                                    <input type="reset" value='Cancle' className='setting-page__setting-form__submit-area__reset' />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        )
    }
}



export default SettingSection