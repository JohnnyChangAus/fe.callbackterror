import React, { Fragment } from 'react';
import styled from "styled-components";
import Item from "./components/Item"

const Navbar = styled.div`
    {
    background-color: white;
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-around;
    height: 100;
    width: 100%;
    }
    @media (max-width: 415px) {
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-between;
    height: 100;
    width: 100%;
    margin-left:30px
    }
`;

const Header = (props) => {
    return(
    <Navbar>
        <Item href="HOME">HOME</Item>
        <Item href="RESUME">ABOUT</Item>
        <Item href="SERVICES">SERVICES</Item>
        <Item href="BLOG">PLANS</Item>
        <Item href="BLOG">BOOK ONLINE</Item>
        <Item href="CONTACT">CONTACT</Item>
    </Navbar>
    );
  };
  
  export default Header;