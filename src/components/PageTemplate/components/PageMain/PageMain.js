import React, { Fragment } from 'react';
import styled from "styled-components";
import './PageMain.css';


const PageInfo = (props) => {
    return(
      <main>

  <div id="c1dmpinlineContent-gridContainer" data-mesh-internal="true">
    <wix-bg-media style={{position: 'fixed', pointerEvents: 'none', width: '100%', height: '100%', top: -80, left: 0, willChange: 'transform', transform: 'translate3d(0px, 149px, 0px)'}} data-fitting="fill" data-align="center" data-container-id="mediait1obidb13" data-page-id="c1dmp" data-media-comp-type="image" data-is-full-height="true" data-bg-effect-name="BgParallax" data-container-size="980, 953" id="mediait1obidb13balatamedia" className="bgMedia">
      <wix-image style={{width: '100%', height: '100%', position: 'absolute', top: 0, left: 0}} data-has-bg-scroll-effect="true" data-image-info="{&quot;imageData&quot;:{&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-iexzux33&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;2_2880px.jpg&quot;,&quot;uri&quot;:&quot;84770f_3f0cee1368ee472ca51f970c1ae7ebbe.jpg&quot;,&quot;description&quot;:&quot;private/allMedia_picture&quot;,&quot;width&quot;:2880,&quot;height&quot;:1918,&quot;alt&quot;:&quot;&quot;,&quot;artist&quot;:{&quot;id&quot;:&quot;&quot;,&quot;name&quot;:&quot;&quot;},&quot;displayMode&quot;:&quot;fill&quot;},&quot;containerId&quot;:&quot;mediait1obidb13&quot;,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" data-bg-effect-name="BgParallax" data-type="image" id="mediait1obidb13balatamediaimage" className="bgImage" data-src="https://static.wixstatic.com/media/84770f_708c3f8d5a1d4ab0bce140e6f0fa95ea.jpg/v1/fill/w_1920,h_1203,al_c,q_85,usm_0.66_1.00_0.01/84770f_708c3f8d5a1d4ab0bce140e6f0fa95ea.webp">
        <img id="mediait1obidb13balatamediaimageimage" style={{width: '100%', height: '100%', objectPosition: '70% 50%', objectFit: 'cover'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/84770f_708c3f8d5a1d4ab0bce140e6f0fa95ea.jpg/v1/fill/w_1920,h_1203,al_c,q_85,usm_0.66_1.00_0.01/84770f_708c3f8d5a1d4ab0bce140e6f0fa95ea.webp" />
      </wix-image>
    </wix-bg-media>

    <div data-packed="true" data-vertical-text="false" style={{width: 432, pointerEvents: 'none'}} className="txtNew" id="iebbgy12">
      <h1 className="font_0" style={{lineHeight: '1.2em'}}>
        <a target="_self">
          <span style={{letterSpacing: '0.1em', color:"white", fontSize: '60px'} }>GALA</span>
        </a>
      </h1>
      <h1 className="font_0" style={{lineHeight: '1.2em'}}>
        <a target="_self">
          <span style={{letterSpacing: '0.1em', color:"white", fontSize: '60px'}}>PERSONAL</span>
        </a>
      </h1>
      <h1 className="font_0" style={{lineHeight: '1.2em'}}>
        <a target="_self">
          <span style={{letterSpacing: '0.1em', color:"white", fontSize: '60px'}}>FITNESS</span>
        </a>
      </h1>
      <h1 className="font_0" style={{lineHeight: '1.2em'}}>
        <a target="_self">
          <span style={{letterSpacing: '0.1em', color:"white", fontSize: '60px'}}>TRAINER</span>
        </a>
      </h1>
    </div>
                
<section style={{left: 0, width: '100%', minWidth: 980, height: 'auto', top: '', bottom: '', right: '', position: '', marginLeft: 0}} data-responsive="true" data-is-screen-width="true" data-col-margin={0} data-row-margin={0} className="strc1" id="comp-if6tmdvp">
  <div style={{position: 'absolute', top: 0, width: 'calc(100% - 0px)', height: '100%', overflow: 'hidden', pointerEvents: 'auto', minWidth: 980, left: 0, right: 0, bottom: 0}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name data-media-type data-use-clip-path data-needs-clipping data-render-type="bolt" className="strc1balata" id="comp-if6tmdvpbalata">
    <div style={{position: 'absolute', width: '100%', height: '100%', top: 0, backgroundColor: 'transparent'}} className="bgColor" id="comp-if6tmdvpbalatabgcolor">
      <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="comp-if6tmdvpbalatabgcoloroverlay" className="bgColoroverlay" />
      </div>
    </div>
  <div style={{position: 'relative', width: 'calc(100% - 0px)', minWidth: 980}} id="comp-if6tmdvpinlineContent" className="strc1inlineContent">
    <div style={{position: 'relative', width: '100%', left: 0, flex: 980, marginLeft: 0, minWidth: 980, top: 0, marginTop: 0, marginBottom: 0, height: '', display: 'flex', bottom: '', right: ''}} data-content-width={980} data-is-mesh="true" className="strc1" id="mediait1obida19">
      <div style={{position: 'absolute', top: 0, width: '100%', height: '100%', overflow: 'hidden', pointerEvents: 'auto', left: 0, right: 0, bottom: 0}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name data-media-type data-use-clip-path data-needs-clipping data-render-type="bolt" className="strc1balata" id="mediait1obida19balata">
        <div style={{position: 'absolute', width: '100%', height: '100%', top: 0, backgroundColor: 'rgba(230, 231, 46, 1)'}} className="bgColor" id="mediait1obida19balatabgcolor">
          <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="mediait1obida19balatabgcoloroverlay" className="bgColoroverlay" />
          </div>
        </div>

        <div style={{width: '100%', position: 'relative', top: 0, bottom: 0}} className="strc1inlineContent" id="mediait1obida19inlineContent">
          <div id="mediait1obida19inlineContent-gridWrapper" data-mesh-internal="true">
            <div id="mediait1obida19inlineContent-gridContainer" data-mesh-internal="true">
              <div data-packed="true" data-vertical-text="false" style={{width: 350, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebbxnfx">
                <h2 style={{textAlign: 'center'}} className="font_2">
                  <span style={{letterSpacing: '0.1em', fontSize: '60px'}}>About</span>
                </h2>
              </div>
              <div data-is-responsive="false" style={{width: 5, height: 65, visibility: 'inherit'}} className="vl1" id="iebbylsx">
                <div id="iebbylsxline" className="vl1line" />
                </div>
              <div data-packed="true" data-vertical-text="false" style={{width: 560, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebc0plc">
                <p style={{textAlign: 'center', lineHeight: '1.4em'}} className="font_8">
                  <span style={{lineHeight: '1.4em'}}>
                    <span id="span1" style={{letterSpacing: '0.05em', fontSize: '15px'}}>I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just click “Edit Text” or double click me to add your own content and make changes to the font. I’m a great place for you to tell a story and let your users know a little more about you.
                    </span>
                  </span>
                </p>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
<div tabIndex={-1} role="region" aria-label="About" style={{left: 0, marginLeft: 0, width: '100%', minWidth: 'initial', top: '', bottom: '', right: '', height: 20, position: ''}} className="AutoWidthAnchorSkin" id="iebef3w2">&nbsp;
</div>
                  
<section style={{left: 0, width: '100%', minWidth: 980, height: 'auto', top: '', bottom: '', right: '', position: '', marginLeft: 0}} data-responsive="true" data-is-screen-width="true" data-col-margin={0} data-row-margin={0} className="strc1" id="comp-iexzu8km">
  <div style={{position: 'absolute', top: 0, width: 'calc(100% - 0px)', height: '100%', overflow: 'hidden', pointerEvents: 'auto', minWidth: 980, left: 0, right: 0, bottom: 0}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name data-media-type data-use-clip-path data-needs-clipping data-render-type="bolt" className="strc1balata" id="comp-iexzu8kmbalata">
    <div style={{position: 'absolute', width: '100%', height: '100%', top: 0, backgroundColor: 'transparent'}} className="bgColor" id="comp-iexzu8kmbalatabgcolor">
      <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="comp-iexzu8kmbalatabgcoloroverlay" className="bgColoroverlay" />
    </div>
  </div>
  <div style={{position: 'relative', width: 'calc(100% - 0px)', minWidth: 980}} id="comp-iexzu8kminlineContent" className="strc1inlineContent">
    <div style={{position: 'relative', width: '100%', left: 0, flex: 980, marginLeft: 0, minWidth: 980, top: 0, marginTop: 0, marginBottom: 0, height: '', display: 'flex', bottom: '', right: ''}} data-content-width={980} data-is-mesh="true" className="strc1" id="mediait1obidb13">
      <div style={{position: 'absolute', top: 0, width: '100%', height: '100%', overflow: 'hidden', pointerEvents: 'none', left: 0, right: 0, bottom: 0, clip: 'rect(0px, auto, 953px, 0px)'}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name="BgParallax" data-media-type="Image" data-use-clip-path data-needs-clipping="true" data-render-type="bolt" className="strc1balata" id="mediait1obidb13balata">
        <div style={{position: 'fixed', width: '100%', height: '100%', top: 0, backgroundColor: 'rgba(255, 255, 255, 1)'}} className="bgColor" id="mediait1obidb13balatabgcolor">
          <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="mediait1obidb13balatabgcoloroverlay" className="bgColoroverlay" />
        </div>
        <wix-bg-media style={{position: 'fixed', pointerEvents: 'none', width: '100%', height: '100%', top: 0, left: 0, willChange: 'transform', transform: 'translate3d(0px, 149px, 0px)'}} data-fitting="fill" data-align="center" data-container-id="mediait1obidb13" data-page-id="c1dmp" data-media-comp-type="image" data-is-full-height="true" data-bg-effect-name="BgParallax" data-container-size="980, 953" id="mediait1obidb13balatamedia" className="bgMedia">
          <wix-image style={{width: '100%', height: '100%', position: 'absolute', top: 0, left: 0}} data-has-bg-scroll-effect="true" data-image-info="{&quot;imageData&quot;:{&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-iexzux33&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;2_2880px.jpg&quot;,&quot;uri&quot;:&quot;84770f_3f0cee1368ee472ca51f970c1ae7ebbe.jpg&quot;,&quot;description&quot;:&quot;private/allMedia_picture&quot;,&quot;width&quot;:2880,&quot;height&quot;:1918,&quot;alt&quot;:&quot;&quot;,&quot;artist&quot;:{&quot;id&quot;:&quot;&quot;,&quot;name&quot;:&quot;&quot;},&quot;displayMode&quot;:&quot;fill&quot;},&quot;containerId&quot;:&quot;mediait1obidb13&quot;,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" data-bg-effect-name="BgParallax" data-type="image" id="mediait1obidb13balatamediaimage" className="bgImage" data-src="https://static.wixstatic.com/media/84770f_3f0cee1368ee472ca51f970c1ae7ebbe.jpg/v1/fill/w_980,h_953,al_c,q_85,usm_0.66_1.00_0.01/84770f_3f0cee1368ee472ca51f970c1ae7ebbe.webp">
            <img id="mediait1obidb13balatamediaimageimage" style={{width: '100%', height: '100%', objectPosition: '0% 50%', objectFit: 'cover'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/84770f_3f0cee1368ee472ca51f970c1ae7ebbe.jpg/v1/fill/w_980,h_953,al_c,q_85,usm_0.66_1.00_0.01/84770f_3f0cee1368ee472ca51f970c1ae7ebbe.webp" />
          </wix-image>
        </wix-bg-media>
      </div>

      <div style={{width: '100%', position: 'relative', top: 0, bottom: 0}} className="strc1inlineContent" id="mediait1obidb13inlineContent">        
        <div id="mediait1obidb13inlineContent-gridWrapper" data-mesh-internal="true">
          <div id="mediait1obidb13inlineContent-gridContainer" data-mesh-internal="true">
            <div data-is-absolute-layout="false" style={{visibility: 'inherit'}} className="c2" id="iebf9n8r">
              <div id="iebf9n8rbg" className="c2bg" />

              <div className="c2inlineContent" id="iebf9n8rinlineContent">
                <div id="iebf9n8rinlineContent-gridWrapper" data-mesh-internal="true">
                  <div id="iebf9n8rinlineContent-gridContainer" data-mesh-internal="true" style={{backgroundColor: 'rgba(253, 208, 193, 1)'}}>
                    <div data-packed="true" data-vertical-text="false" style={{width: 349, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebfcjcq">
                      <h5 className="font_5" style={{lineHeight: '1.2em', textAlign: 'center'}}>
                        <span style={{letterSpacing: '1px', fontSize: '30px'}}>GROUP TRAINING</span>
                      </h5>
                    </div>
                    <div data-packed="true" data-vertical-text="false" style={{width: 270, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebfcjcp">
                      <p style={{textAlign: 'center', lineHeight: '1.4em'}} className="font_8">
                        <span style={{lineHeight: '1.4em'}}>
                          <span style={{letterSpacing: '0.05em', fontSize: '15px'}}>I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just click “Edit Text” or double click me to add your own content and make changes to the font.
                          </span>
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>

            </div>    
            <div id="mediait1obidb13inlineContent-wedge-3" data-mesh-internal="true" />
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
                    
  <div data-is-absolute-layout="false" style={{visibility: 'inherit'}} className="c4" id="iebfbshu">
    <div id="iebfbshubg" className="c4bg" />

    <div className="c4inlineContent" id="iebfbshuinlineContent">
      <div id="iebfbshuinlineContent-gridWrapper" data-mesh-internal="true">
        <div id="iebfbshuinlineContent-gridContainer" data-mesh-internal="true" style={{backgroundColor: 'rgba(130, 228, 213, 1)'}}>
          <div data-packed="true" data-vertical-text="false" style={{width: 350, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebf56us">
            <h5 className="font_5" style={{lineHeight: '1.2em', textAlign: 'center'}}>
              <span style={{letterSpacing: '1px', fontSize: '30px'}}>PERSONAL TRAINING</span>
            </h5>
          </div>
        <div data-packed="true" data-vertical-text="false" style={{width: 270, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebf7c2q">
          <p style={{textAlign: 'center', lineHeight: '1.4em'}} className="font_8">
            <span style={{lineHeight: '1.4em'}}>
              <span style={{letterSpacing: '0.05em',fontSize: '15px'}}>I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just click “Edit Text” or double click me to add your own content and make changes to the font.
              </span>
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
  
</div>
                  
  <section style={{left: 0, width: '100%', minWidth: 980, height: 'auto', top: '', bottom: '', right: '', position: '', marginLeft: 0}} data-responsive="true" data-is-screen-width="true" data-col-margin={0} data-row-margin={0} className="strc1" id="comp-if6tnksz">
    <div style={{position: 'absolute', top: 0, width: 'calc(100% - 0px)', height: '100%', overflow: 'hidden', pointerEvents: 'auto', minWidth: 980, left: 0, right: 0, bottom: 0}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name data-media-type data-use-clip-path data-needs-clipping data-render-type="bolt" className="strc1balata" id="comp-if6tnkszbalata">
      <div style={{position: 'absolute', width: '100%', height: '100%', top: 0, backgroundColor: 'transparent'}} className="bgColor" id="comp-if6tnkszbalatabgcolor">
        <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="comp-if6tnkszbalatabgcoloroverlay" className="bgColoroverlay" />
        </div>
      </div>
      <div style={{position: 'relative', width: 'calc(100% - 0px)', minWidth: 980}} id="comp-if6tnkszinlineContent" className="strc1inlineContent">
        <div style={{position: 'relative', width: '100%', left: 0, flex: 980, marginLeft: 0, minWidth: 980, top: 0, marginTop: 0, marginBottom: 0, height: '', display: 'flex', bottom: '', right: ''}} data-content-width={980} data-is-mesh="true" className="strc1" id="mediait1obida13">
          <div style={{position: 'absolute', top: 0, width: '100%', height: '100%', overflow: 'hidden', pointerEvents: 'auto', left: 0, right: 0, bottom: 0}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name data-media-type data-use-clip-path data-needs-clipping data-render-type="bolt" className="strc1balata" id="mediait1obida13balata">
            <div style={{position: 'absolute', width: '100%', height: '100%', top: 0, backgroundColor: 'rgba(253, 208, 193, 1)'}} className="bgColor" id="mediait1obida13balatabgcolor">
              <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="mediait1obida13balatabgcoloroverlay" className="bgColoroverlay" />
              </div>
            </div>
            <div style={{width: '100%', position: 'relative', top: 0, bottom: 0}} className="strc1inlineContent" id="mediait1obida13inlineContent">

              <div id="mediait1obida13inlineContent-gridWrapper" data-mesh-internal="true">
                <div id="mediait1obida13inlineContent-gridContainer" data-mesh-internal="true">
                  <div data-packed="true" data-vertical-text="false" style={{width: 350, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebcr8mn">
                    <h2 className="font_2" style={{textAlign: 'center'}}>
                      <span style={{letterSpacing: '0.1em', fontSize: '60px'}}>Services</span>
                    </h2>
                  </div>
                <div data-is-responsive="false" style={{width: 5, height: 65, visibility: 'inherit'}} className="vl1" id="iebcr8mo">
              <div id="iebcr8moline" className="vl1line" />
            </div>
            <div data-packed="true" data-vertical-text="false" style={{width: 560, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebcr8mo_0">
              <p style={{textAlign: 'center', lineHeight: '1.4em'}} className="font_8">
                <span style={{lineHeight: '1.4em'}}>
                  <span style={{letterSpacing: '0.05em', fontSize: '15px'}}>I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just click “Edit Text” or double click me to add your own content and make changes to the font. I’m a great place for you to tell a story and let your users know a little more about you.
                  </span>
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
                
<div tabIndex={-1} role="region" aria-label="Services" style={{left: 0, marginLeft: 0, width: '100%', minWidth: 'initial', top: '', bottom: '', right: '', height: 20, position: ''}} className="AutoWidthAnchorSkin" id="iebefvgs">
  &nbsp;
</div>
                
  <section style={{left: 0, width: '100%', minWidth: 980, height: 'auto', top: '', bottom: '', right: '', position: '', marginLeft: 0}} data-responsive="true" data-is-screen-width="true" data-col-margin={0} data-row-margin={0} className="strc1" id="comp-if6tp4mu">
    <div style={{position: 'absolute', top: 0, width: 'calc(100% - 0px)', height: '100%', overflow: 'hidden', pointerEvents: 'auto', minWidth: 980, left: 0, right: 0, bottom: 0}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name data-media-type data-use-clip-path data-needs-clipping data-render-type="bolt" className="strc1balata" id="comp-if6tp4mubalata">
      <div style={{position: 'absolute', width: '100%', height: '100%', top: 0, backgroundColor: 'transparent'}} className="bgColor" id="comp-if6tp4mubalatabgcolor">
        <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="comp-if6tp4mubalatabgcoloroverlay" className="bgColoroverlay" />
        </div>
      </div>
        <div style={{position: 'relative', width: 'calc(100% - 0px)', minWidth: 980}} id="comp-if6tp4muinlineContent" className="strc1inlineContent">
          <div style={{position: 'relative', width: '100%', left: 0, flex: 980, marginLeft: 0, minWidth: 980, top: 0, marginTop: 0, marginBottom: 0, height: '', display: 'flex', bottom: '', right: ''}} data-content-width={980} data-is-mesh="true" className="strc1" id="mediait1obida1">
            <div style={{position: 'absolute', top: 0, width: '100%', height: '100%', overflow: 'hidden', pointerEvents: 'auto', left: 0, right: 0, bottom: 0}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name data-media-type="Image" data-use-clip-path data-needs-clipping data-render-type="bolt" className="strc1balata" id="mediait1obida1balata">
              <div style={{position: 'absolute', width: '100%', height: '100%', top: 0, backgroundColor: 'rgba(36, 35, 35, 1)'}} className="bgColor" id="mediait1obida1balatabgcolor">
                <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="mediait1obida1balatabgcoloroverlay" className="bgColoroverlay" />
              </div>
              <div style={{position: 'absolute', pointerEvents: 'auto', width: '100%', height: '100%', top: 0, left: 0}} data-fitting="fill" data-align="center" data-container-id="mediait1obida1" data-page-id="c1dmp" data-media-comp-type="image" className="bgMedia" id="mediait1obida1balatamedia">
                <wix-image style={{width: '100%', height: '100%', position: 'absolute', top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-if6txg8i&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;ZC6A3176_c.jpg&quot;,&quot;uri&quot;:&quot;84770f_697adeafbb59449b9921f8fe484cbf7d.jpg&quot;,&quot;description&quot;:&quot;private/allMedia_picture&quot;,&quot;width&quot;:1500,&quot;height&quot;:819,&quot;alt&quot;:&quot;&quot;,&quot;artist&quot;:{&quot;id&quot;:&quot;&quot;,&quot;name&quot;:&quot;&quot;},&quot;originalImageDataRef&quot;:{&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-if6tuvfa&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;ZC6A3176_c.jpg&quot;,&quot;uri&quot;:&quot;84770f_2a1acb622824463e8450e1313c0b61b9.jpg&quot;,&quot;description&quot;:&quot;private/allMedia_picture&quot;,&quot;width&quot;:4096,&quot;height&quot;:2237,&quot;alt&quot;:&quot;&quot;,&quot;artist&quot;:{&quot;id&quot;:&quot;&quot;,&quot;name&quot;:&quot;&quot;}},&quot;displayMode&quot;:&quot;fill&quot;},&quot;containerId&quot;:&quot;mediait1obida1&quot;,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" data-type="image" id="mediait1obida1balatamediaimage" className="bgImage" data-src="https://static.wixstatic.com/media/84770f_697adeafbb59449b9921f8fe484cbf7d.jpg/v1/fill/w_944,h_819,al_c,q_85/84770f_697adeafbb59449b9921f8fe484cbf7d.webp">
                  <img id="mediait1obida1balatamediaimageimage" style={{width: '100%', height: '100%', objectPosition: '50% 50%', objectFit: 'cover'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/84770f_697adeafbb59449b9921f8fe484cbf7d.jpg/v1/fill/w_944,h_819,al_c,q_85/84770f_697adeafbb59449b9921f8fe484cbf7d.webp" />
                </wix-image>
              </div>
            </div>
            <div style={{width: '100%', position: 'relative', top: 0, bottom: 0}} className="strc1inlineContent" id="mediait1obida1inlineContent">
                <div id="mediait1obida1inlineContent-gridWrapper" data-mesh-internal="true">
                  <div id="mediait1obida1inlineContent-gridContainer" data-mesh-internal="true">
                    <div style={{width: 197, height: 197, visibility: 'inherit'}} className="s_DIkImageButtonSkin" data-state="supports_opacity transition_none prepare_hda  " id="iebc6yh6">
                      <a target="_self" data-keep-roots="true" data-anchor-comp-id data-anchor="dataItem-ifp0738g" title style={{width: 197, height: 197}} id="iebc6yh6link" className="s_DIkImageButtonSkinlink"><div className="s_DIkImageButtonSkin_correct-positioning">
                        <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_defaultImage17i8&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_white.png&quot;,&quot;uri&quot;:&quot;a896d9_b914721e90cc47aca6f08411986fbedb.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebc6yh6&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebc6yh6defaultImage" className="s_DIkImageButtonSkindefaultImage" data-src="https://static.wixstatic.com/media/a896d9_b914721e90cc47aca6f08411986fbedb.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_b914721e90cc47aca6f08411986fbedb.webp">
                          <img id="iebc6yh6defaultImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_b914721e90cc47aca6f08411986fbedb.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_b914721e90cc47aca6f08411986fbedb.webp" />
                        </wix-image>
                      </div>
                      <div className="s_DIkImageButtonSkin_correct-positioning">
                        <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_hoverImage18r7&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_blue.png&quot;,&quot;uri&quot;:&quot;a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebc6yh6&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebc6yh6hoverImage" className="s_DIkImageButtonSkinhoverImage" data-src="https://static.wixstatic.com/media/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.webp"><img id="iebc6yh6hoverImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.webp" />
                        </wix-image>
                      </div>
                      <div className="s_DIkImageButtonSkin_correct-positioning">
                        <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_activeImage1ft8&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_white.png&quot;,&quot;uri&quot;:&quot;a896d9_9dbe0146c6674f06b11dbc411687ae35.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:600,&quot;height&quot;:600,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebc6yh6&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebc6yh6activeImage" className="s_DIkImageButtonSkinactiveImage" data-src="https://static.wixstatic.com/media/a896d9_9dbe0146c6674f06b11dbc411687ae35.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_9dbe0146c6674f06b11dbc411687ae35.webp">
                        </wix-image>
                      </div>
                    </a>
                  </div>
                  <div data-packed="true" data-vertical-text="false" style={{width: 153, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebck7pt">
                    <h5 style={{textAlign: 'center', lineHeight: '1.2em'}} className="font_5">
                      <a target="_self" data-anchor="dataItem-ifp0738g">
                        <span style={{lineHeight: '2em'}}>
                          <span style={{letterSpacing: '0.1em', fontSize: '20px',color: 'black' }}>TRX WORKOUT
                          </span>
                        </span>
                      </a>
                    </h5>
                  </div>
                  <div style={{width: 197, height: 197, visibility: 'inherit'}} className="ImageButton_1" data-state="supports_opacity transition_none prepare_hda  " id="iebcfrd6">
                    <a target="_self" data-keep-roots="true" data-anchor-comp-id data-anchor="dataItem-ifp0738g" title style={{width: 197, height: 197}} id="iebcfrd6link" className="ImageButton_1link">
                      <div className="ImageButton_1_correct-positioning">
                        <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_defaultImageq8z&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_pink.png&quot;,&quot;uri&quot;:&quot;a896d9_a67b455c6d2d4ad3bd82ae9d0eadd73c.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcfrd6&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcfrd6defaultImage" className="ImageButton_1defaultImage" data-src="https://static.wixstatic.com/media/a896d9_a67b455c6d2d4ad3bd82ae9d0eadd73c.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_a67b455c6d2d4ad3bd82ae9d0eadd73c.webp">
                          <img id="iebcfrd6defaultImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_a67b455c6d2d4ad3bd82ae9d0eadd73c.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_a67b455c6d2d4ad3bd82ae9d0eadd73c.webp" />
                        </wix-image>
                      </div>
                    <div className="ImageButton_1_correct-positioning">
                      <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_hoverImage1hoa&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_blue.png&quot;,&quot;uri&quot;:&quot;a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcfrd6&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcfrd6hoverImage" className="ImageButton_1hoverImage" data-src="https://static.wixstatic.com/media/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.webp"><img id="iebcfrd6hoverImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.webp" />
                      </wix-image>
                    </div>
                    <div className="ImageButton_1_correct-positioning">
                      <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_activeImage8on&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_white.png&quot;,&quot;uri&quot;:&quot;a896d9_9dbe0146c6674f06b11dbc411687ae35.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:600,&quot;height&quot;:600,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcfrd6&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcfrd6activeImage" className="ImageButton_1activeImage" data-src="https://static.wixstatic.com/media/a896d9_9dbe0146c6674f06b11dbc411687ae35.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_9dbe0146c6674f06b11dbc411687ae35.webp">
                        <img id="iebcfrd6activeImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_9dbe0146c6674f06b11dbc411687ae35.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_9dbe0146c6674f06b11dbc411687ae35.webp" />
                      </wix-image>
                    </div>
                  </a>
                </div>
                  <div data-packed="true" data-vertical-text="false" style={{width: 141, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebcm5v0">
                    <h5 className="font_5" style={{lineHeight: '1.2em', textAlign: 'center'}}>
                      <a target="_self" data-anchor="dataItem-ifp0738g">
                        <span style={{lineHeight: '2em'}}>
                          <span style={{letterSpacing: '0.1em', fontSize: '20px', color: 'black' }}>WEIGHT LOSS
                          </span>
                        </span>
                      </a>
                    </h5>
                  </div>
                  <div style={{width: 197, height: 197, visibility: 'inherit'}} className="ImageButton_1" data-state="supports_opacity transition_none   " id="iebcgc41">
                    <a target="_self" data-keep-roots="true" data-anchor-comp-id data-anchor="dataItem-ifp0738g" title style={{width: 197, height: 197}} id="iebcgc41link" className="ImageButton_1link">
                    <div className="ImageButton_1_correct-positioning">
                      <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_defaultImagerem&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_white.png&quot;,&quot;uri&quot;:&quot;a896d9_b914721e90cc47aca6f08411986fbedb.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcgc41&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcgc41defaultImage" className="ImageButton_1defaultImage" data-src="https://static.wixstatic.com/media/a896d9_b914721e90cc47aca6f08411986fbedb.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_b914721e90cc47aca6f08411986fbedb.webp"><img id="iebcgc41defaultImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_b914721e90cc47aca6f08411986fbedb.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_b914721e90cc47aca6f08411986fbedb.webp" />
                      </wix-image>
                    </div>
                  <div className="ImageButton_1_correct-positioning">
                    <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_hoverImage24o5&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_blue.png&quot;,&quot;uri&quot;:&quot;a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcgc41&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcgc41hoverImage" className="ImageButton_1hoverImage" data-src="https://static.wixstatic.com/media/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.webp">
                      <img id="iebcgc41hoverImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.webp" />
                    </wix-image>
                  </div>
                  <div className="ImageButton_1_correct-positioning">
                    <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_activeImage1hac&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_white.png&quot;,&quot;uri&quot;:&quot;a896d9_9dbe0146c6674f06b11dbc411687ae35.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:600,&quot;height&quot;:600,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcgc41&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcgc41activeImage" className="ImageButton_1activeImage" data-src="https://static.wixstatic.com/media/a896d9_9dbe0146c6674f06b11dbc411687ae35.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_9dbe0146c6674f06b11dbc411687ae35.webp"><img id="iebcgc41activeImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_9dbe0146c6674f06b11dbc411687ae35.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_9dbe0146c6674f06b11dbc411687ae35.webp" />
                    </wix-image>
                  </div>
                </a>
              </div>
              <div data-packed="true" data-vertical-text="false" style={{width: 147, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebcmn6q">
                <h5 style={{textAlign: 'center', lineHeight: '1.2em'}} className="font_5">
                  <a target="_self" data-anchor="dataItem-ifp0738g">
                    <span style={{lineHeight: '2em'}}>
                      <span style={{letterSpacing: '0.1em', fontSize: '20px', color: 'black'}}>MUSCLE TONE
                      </span>
                    </span>
                  </a>
                </h5>
              </div>
              <div style={{width: 197, height: 197, visibility: 'inherit'}} className="ImageButton_1" data-state="supports_opacity transition_none prepare_hda  " id="iebcgqni">
                <a target="_self" data-keep-roots="true" data-anchor-comp-id data-anchor="dataItem-ifp0738g" title style={{width: 197, height: 197}} id="iebcgqnilink" className="ImageButton_1link">
                  <div className="ImageButton_1_correct-positioning">
                    <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_defaultImage1hec&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_white.png&quot;,&quot;uri&quot;:&quot;a896d9_b914721e90cc47aca6f08411986fbedb.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcgqni&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcgqnidefaultImage" className="ImageButton_1defaultImage" data-src="https://static.wixstatic.com/media/a896d9_b914721e90cc47aca6f08411986fbedb.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_b914721e90cc47aca6f08411986fbedb.webp">
                      <img id="iebcgqnidefaultImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_b914721e90cc47aca6f08411986fbedb.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_b914721e90cc47aca6f08411986fbedb.webp" />
                    </wix-image>
                  </div>
                  <div className="ImageButton_1_correct-positioning">
                    <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_hoverImagewga&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_blue.png&quot;,&quot;uri&quot;:&quot;a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcgqni&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcgqnihoverImage" className="ImageButton_1hoverImage" data-src="https://static.wixstatic.com/media/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.webp">
                      <img id="iebcgqnihoverImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.webp" />
                    </wix-image>
                  </div>
                  <div className="ImageButton_1_correct-positioning">
                    <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_activeImage189q&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_white.png&quot;,&quot;uri&quot;:&quot;a896d9_9dbe0146c6674f06b11dbc411687ae35.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:600,&quot;height&quot;:600,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcgqni&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcgqniactiveImage" className="ImageButton_1activeImage" data-src="https://static.wixstatic.com/media/a896d9_9dbe0146c6674f06b11dbc411687ae35.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_9dbe0146c6674f06b11dbc411687ae35.webp">
                    </wix-image>
                  </div>
                </a>
              </div>
              <div data-packed="true" data-vertical-text="false" style={{width: 147, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="comp-ifpc9tm5">
                <h5 style={{textAlign: 'center', lineHeight: '1.2em'}} className="font_5">
                  <a target="_self" data-anchor="dataItem-ifp0738g">
                    <span style={{lineHeight: '2em'}}>
                      <span style={{letterSpacing: '0.1em', fontSize: '20px', color: 'black'}}>POSTURE CORRECTION
                      </span>
                    </span>
                  </a>
                </h5>
              </div>
              <div style={{width: 197, height: 197, visibility: 'inherit'}} className="ImageButton_1" data-state="supports_opacity transition_none prepare_hda  " id="iebcgu2b">
                <a target="_self" data-keep-roots="true" data-anchor-comp-id data-anchor="dataItem-ifp0738g" title style={{width: 197, height: 197}} id="iebcgu2blink" className="ImageButton_1link"><div className="ImageButton_1_correct-positioning">
                <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_defaultImagey8x&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_yellow.png&quot;,&quot;uri&quot;:&quot;a896d9_cb70a2da6d7e4fc7a21a41a66bad7cba.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcgu2b&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcgu2bdefaultImage" className="ImageButton_1defaultImage" data-src="https://static.wixstatic.com/media/a896d9_cb70a2da6d7e4fc7a21a41a66bad7cba.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_cb70a2da6d7e4fc7a21a41a66bad7cba.webp">
                  <img id="iebcgu2bdefaultImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_cb70a2da6d7e4fc7a21a41a66bad7cba.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_cb70a2da6d7e4fc7a21a41a66bad7cba.webp" />
                </wix-image>
              </div>
              <div className="ImageButton_1_correct-positioning">
                <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_hoverImage1adf&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_blue.png&quot;,&quot;uri&quot;:&quot;a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcgu2b&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcgu2bhoverImage" className="ImageButton_1hoverImage" data-src="https://static.wixstatic.com/media/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.webp">
                  <img id="iebcgu2bhoverImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.webp" />
                </wix-image>
              </div>
                <div className="ImageButton_1_correct-positioning">
                  <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_activeImagendz&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_yellow.png&quot;,&quot;uri&quot;:&quot;a896d9_cb70a2da6d7e4fc7a21a41a66bad7cba.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcgu2b&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcgu2bactiveImage" className="ImageButton_1activeImage" data-src="https://static.wixstatic.com/media/a896d9_cb70a2da6d7e4fc7a21a41a66bad7cba.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_cb70a2da6d7e4fc7a21a41a66bad7cba.webp"><img id="iebcgu2bactiveImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_cb70a2da6d7e4fc7a21a41a66bad7cba.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_cb70a2da6d7e4fc7a21a41a66bad7cba.webp" />
                  </wix-image>
                </div>
              </a>
            </div>
            <div data-packed="true" data-vertical-text="false" style={{width: 147, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="comp-ifpcbgup">
              <h5 style={{textAlign: 'center', lineHeight: '1.2em'}} className="font_5">
                <a target="_self" data-anchor="dataItem-ifp0738g">
                  <span style={{lineHeight: '2em'}}>
                    <span style={{letterSpacing: '0.1em', fontSize: '20px', color: 'black'}}>CARDIO FITNESS
                    </span>
                  </span>
                </a>
              </h5>
            </div>
            <div style={{width: 197, height: 197, visibility: 'inherit'}} className="ImageButton_1" data-state="supports_opacity transition_none   " id="iebcgxhf">
              <a target="_self" data-keep-roots="true" data-anchor-comp-id data-anchor="dataItem-ifp0738g" title style={{width: 197, height: 197}} id="iebcgxhflink" className="ImageButton_1link">
                <div className="ImageButton_1_correct-positioning">
                  <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_defaultImage1af6&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_white.png&quot;,&quot;uri&quot;:&quot;a896d9_b914721e90cc47aca6f08411986fbedb.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcgxhf&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcgxhfdefaultImage" className="ImageButton_1defaultImage" data-src="https://static.wixstatic.com/media/a896d9_b914721e90cc47aca6f08411986fbedb.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_b914721e90cc47aca6f08411986fbedb.webp">
                    <img id="iebcgxhfdefaultImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_b914721e90cc47aca6f08411986fbedb.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_b914721e90cc47aca6f08411986fbedb.webp" />
                  </wix-image>
                </div>
                <div className="ImageButton_1_correct-positioning">
                  <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_hoverImage106s&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_blue.png&quot;,&quot;uri&quot;:&quot;a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:426,&quot;height&quot;:426,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcgxhf&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcgxhfhoverImage" className="ImageButton_1hoverImage" data-src="https://static.wixstatic.com/media/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.webp">
                    <img id="iebcgxhfhoverImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_14fcdd8cc1ae4807bbc77aaabe00cfd6.webp" />
                  </wix-image>
                </div>
                <div className="ImageButton_1_correct-positioning">
                  <wix-image style={{width: 197, height: 197, top: 0, left: 0}} data-has-bg-scroll-effect data-image-info="{&quot;imageData&quot;:{&quot;alt&quot;:&quot;&quot;,&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;c_activeImagep0z&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;square_white.png&quot;,&quot;uri&quot;:&quot;a896d9_9dbe0146c6674f06b11dbc411687ae35.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:600,&quot;height&quot;:600,&quot;displayMode&quot;:&quot;full&quot;},&quot;containerId&quot;:&quot;iebcgxhf&quot;,&quot;displayMode&quot;:&quot;full&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" id="iebcgxhfactiveImage" className="ImageButton_1activeImage" data-src="https://static.wixstatic.com/media/a896d9_9dbe0146c6674f06b11dbc411687ae35.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_9dbe0146c6674f06b11dbc411687ae35.webp">
                    <img id="iebcgxhfactiveImageimage" style={{objectPosition: 'center center', width: 197, height: 197, objectFit: 'contain'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/a896d9_9dbe0146c6674f06b11dbc411687ae35.png/v1/fill/w_394,h_394,al_c,q_85,usm_0.66_1.00_0.01/a896d9_9dbe0146c6674f06b11dbc411687ae35.webp" />
                  </wix-image>
                </div>
              </a>
            </div>
            <div data-packed="true" data-vertical-text="false" style={{width: 157, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebcpsnf">
            <h5 className="font_5" style={{lineHeight: '1.2em', textAlign: 'center',}}>
              <a target="_self" data-anchor="dataItem-ifp0738g">
                <span style={{lineHeight: '2em'}}>
                  <span style={{letterSpacing: '0.1em', fontSize: '20px', color: 'black'}}>CORE STRENGTH
                  </span>
                </span>
              </a>
            </h5>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<section style={{left: 0, width: '100%', minWidth: 980, height: 'auto', top: '', bottom: '', right: '', position: '', marginLeft: 0}} data-responsive="true" data-is-screen-width="true" data-col-margin={0} data-row-margin={0} className="strc1" id="comp-iexzvrye">
  <div style={{position: 'absolute', top: 0, width: 'calc(100% - 0px)', height: '100%', overflow: 'hidden', pointerEvents: 'auto', minWidth: 980, left: 0, right: 0, bottom: 0}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name data-media-type data-use-clip-path data-needs-clipping data-render-type="bolt" className="strc1balata" id="comp-iexzvryebalata">
    <div style={{position: 'absolute', width: '100%', height: '100%', top: 0, backgroundColor: 'transparent'}} className="bgColor" id="comp-iexzvryebalatabgcolor">
      <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="comp-iexzvryebalatabgcoloroverlay" className="bgColoroverlay" />
    </div>
  </div>
  <div style={{position: 'relative', width: 'calc(100% - 0px)', minWidth: 980}} id="comp-iexzvryeinlineContent" className="strc1inlineContent">
    <div style={{position: 'relative', width: '100%', left: 0, flex: 980, marginLeft: 0, minWidth: 980, top: 0, marginTop: 0, marginBottom: 0, height: '', display: 'flex', bottom: '', right: ''}} data-content-width={980} data-is-mesh="true" className="strc1" id="mediait1obidb7">
      <div style={{position: 'absolute', top: 0, width: '100%', height: '100%', overflow: 'hidden', pointerEvents: 'none', left: 0, right: 0, bottom: 0, clip: 'rect(0px, auto, 735px, 0px)'}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name="BgParallax" data-media-type="Image" data-use-clip-path data-needs-clipping="true" data-render-type="bolt" className="strc1balata" id="mediait1obidb7balata">
        <div style={{position: 'fixed', width: '100%', height: '100%', top: 0, backgroundColor: 'rgba(255, 255, 255, 1)'}} className="bgColor" id="mediait1obidb7balatabgcolor">
          <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="mediait1obidb7balatabgcoloroverlay" className="bgColoroverlay" />
        </div>
        <wix-bg-media style={{position: 'fixed', pointerEvents: 'none', width: '100%', height: '100%', top: 0, left: 0, willChange: 'transform', transform: 'translate3d(0px, 149px, 0px)'}} data-fitting="fill" data-align="center" data-container-id="mediait1obidb7" data-page-id="c1dmp" data-media-comp-type="image" data-is-full-height="true" data-bg-effect-name="BgParallax" data-container-size="980, 735" id="mediait1obidb7balatamedia" className="bgMedia">
          <wix-image style={{width: '100%', height: '100%', position: 'absolute', top: 0, left: 0}} data-has-bg-scroll-effect="true" data-image-info="{&quot;imageData&quot;:{&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-iey2ge0k&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;strip_fit.jpg&quot;,&quot;uri&quot;:&quot;84770f_98e3c16564e8460f928638971295c790.jpg&quot;,&quot;description&quot;:&quot;private/allMedia_picture&quot;,&quot;width&quot;:1920,&quot;height&quot;:1080,&quot;alt&quot;:&quot;&quot;,&quot;artist&quot;:{&quot;id&quot;:&quot;&quot;,&quot;name&quot;:&quot;&quot;},&quot;displayMode&quot;:&quot;fill&quot;},&quot;containerId&quot;:&quot;mediait1obidb7&quot;,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" data-bg-effect-name="BgParallax" data-type="image" id="mediait1obidb7balatamediaimage" className="bgImage" data-src="https://static.wixstatic.com/media/84770f_98e3c16564e8460f928638971295c790.jpg/v1/fill/w_980,h_745,al_c,q_85,usm_0.66_1.00_0.01/84770f_98e3c16564e8460f928638971295c790.webp">
            <img id="mediait1obidb7balatamediaimageimage" style={{width: '100%', height: '100%', objectPosition: '100% 50%', objectFit: 'cover'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/84770f_98e3c16564e8460f928638971295c790.jpg/v1/fill/w_980,h_745,al_c,q_85,usm_0.66_1.00_0.01/84770f_98e3c16564e8460f928638971295c790.webp" />
          </wix-image>
        </wix-bg-media>
      </div>
      <div style={{width: '100%', position: 'relative', top: 0, bottom: 0}} className="strc1inlineContent" id="mediait1obidb7inlineContent">
        <div id="mediait1obidb7inlineContent-gridWrapper" data-mesh-internal="true">
          <div id="mediait1obidb7inlineContent-gridContainer" data-mesh-internal="true">
            <wix-iframe style={{width: 692, height: 488, overflow: 'hidden', minHeight: 488, minWidth: 692}} data-has-iframe="true" data-src="https://wixlabs-get-my-app.appspot.com/widget?cacheKiller=1605634731691&commonConfig=%7B%7BcommonConfig%7D%7D&compId=comp-jx04ts2m&consent-policy=%7B%7BconsentPolicy%7D%7D&currency=USD&deviceType=desktop&height=488&instance=b4WvSLLkWFu0iuGH2Yu85b4U-oh3IRkK36kt0uU1bkw.eyJpbnN0YW5jZUlkIjoiMGU0OTI2Y2ItOGQzMC00YzlkLTkyZmYtMjc0NmM2ZWU1MGQzIiwiYXBwRGVmSWQiOiIxNDBkMDI0MS1hYTY2LWU1ODMtMTg5MC04NjhjYmJiYjhlNGYiLCJtZXRhU2l0ZUlkIjoiNTgwZjliNDAtYmQzZC00NWZlLWI0ZWEtOWVhNzA5YjliZTUwIiwic2lnbkRhdGUiOiIyMDIwLTExLTE3VDE3OjQ1OjIwLjM4M1oiLCJkZW1vTW9kZSI6ZmFsc2UsIm9yaWdpbkluc3RhbmNlSWQiOiIzNWQwMjRlYy03MjBhLTRjMTYtOGI0OS0xYTU5MzRjZmJhNzgiLCJiaVRva2VuIjoiNTY0NmJkOGItMzAwZC0wOTYzLTI2MTUtYjllMWNmNTdlZTgzIiwic2l0ZU93bmVySWQiOiI4NDc3MGY2Ny1lY2JkLTQ0YjYtYjM1YS01ODRmMmRjMTVhZjEiLCJjYWNoZSI6dHJ1ZSwic2l0ZUlzVGVtcGxhdGUiOnRydWV9&locale=en&pageId=c1dmp&siteRevision=264&tz=America%2FLos_Angeles&viewMode=site&viewerCompId=comp-jx04ts2m&width=692" data-is-tpa="true" data-widget-id="4c0aeeee-e17a-4d96-9766-f8758c0aeb60" data-app-definition-id="140d0241-aa66-e583-1890-868cbbbb8e4f" id="comp-jx04ts2m" className="style-jx04txzv">
              <iframe scrolling="no" frameBorder={0} allow="autoplay; camera; microphone; geolocation; vr" allowTransparency="true" allowFullScreen name="comp-jx04ts2m" style={{width: 692, height: 488, minHeight: 488, minWidth: 692, display: 'block', position: 'absolute', zIndex: ''}} title="Download My App" aria-label="Download My App" id="comp-jx04ts2miframe" className="style-jx04txzviframe" src="https://wixlabs-get-my-app.appspot.com/widget?cacheKiller=1605634731691&commonConfig=%7B%22brand%22%3A%22wix%22%2C%22consentPolicy%22%3A%7B%22essential%22%3Atrue%2C%22functional%22%3Atrue%2C%22analytics%22%3Atrue%2C%22advertising%22%3Afalse%2C%22dataToThirdParty%22%3Afalse%7D%2C%22consentPolicyHeader%22%3A%7B%22consent-policy%22%3A%22%257B%2522func%2522%253A1%252C%2522anl%2522%253A1%252C%2522adv%2522%253A0%252C%2522dt3%2522%253A0%252C%2522ess%2522%253A1%257D%22%7D%2C%22bsi%22%3A%22612f1063-cecc-4c7b-9587-f3fdf06edec3%7C1%22%7D&compId=comp-jx04ts2m&consent-policy=%7B%22func%22%3A1%2C%22anl%22%3A1%2C%22adv%22%3A0%2C%22dt3%22%3A0%2C%22ess%22%3A1%7D&currency=USD&deviceType=desktop&height=488&instance=mMDUs26GP-ZVZuPxKLN1Hq_v_lWZ41aRBy9picDhNVc.eyJpbnN0YW5jZUlkIjoiMGU0OTI2Y2ItOGQzMC00YzlkLTkyZmYtMjc0NmM2ZWU1MGQzIiwiYXBwRGVmSWQiOiIxNDBkMDI0MS1hYTY2LWU1ODMtMTg5MC04NjhjYmJiYjhlNGYiLCJtZXRhU2l0ZUlkIjoiNTgwZjliNDAtYmQzZC00NWZlLWI0ZWEtOWVhNzA5YjliZTUwIiwic2lnbkRhdGUiOiIyMDIwLTExLTIzVDAzOjI4OjIzLjk5OVoiLCJkZW1vTW9kZSI6ZmFsc2UsIm9yaWdpbkluc3RhbmNlSWQiOiIzNWQwMjRlYy03MjBhLTRjMTYtOGI0OS0xYTU5MzRjZmJhNzgiLCJhaWQiOiJjNDQ2MjVmYi1hMjc4LTRhNDktYTRlNy0wMDRiZjBlNmY1ODUiLCJiaVRva2VuIjoiNTY0NmJkOGItMzAwZC0wOTYzLTI2MTUtYjllMWNmNTdlZTgzIiwic2l0ZU93bmVySWQiOiI4NDc3MGY2Ny1lY2JkLTQ0YjYtYjM1YS01ODRmMmRjMTVhZjEiLCJzaXRlSXNUZW1wbGF0ZSI6dHJ1ZX0&locale=en&pageId=c1dmp&siteRevision=264&tz=America%2FLos_Angeles&viewMode=site&viewerCompId=comp-jx04ts2m&width=692" />
              <div id="comp-jx04ts2moverlay" className="style-jx04txzvoverlay" />
            </wix-iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section style={{left: 0, width: '100%', minWidth: 980, height: 'auto', top: '', bottom: '', right: '', position: '', marginLeft: 0}} data-responsive="true" data-is-screen-width="true" data-col-margin={0} data-row-margin={0} className="strc1" id="comp-if6tyiyc">
  <div style={{position: 'absolute', top: 0, width: 'calc(100% - 0px)', height: '100%', overflow: 'hidden', pointerEvents: 'auto', minWidth: 980, left: 0, right: 0, bottom: 0}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name data-media-type data-use-clip-path data-needs-clipping data-render-type="bolt" className="strc1balata" id="comp-if6tyiycbalata">
    <div style={{position: 'absolute', width: '100%', height: '100%', top: 0, backgroundColor: 'transparent'}} className="bgColor" id="comp-if6tyiycbalatabgcolor">
      <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="comp-if6tyiycbalatabgcoloroverlay" className="bgColoroverlay" />
    </div>
  </div>
  <div style={{position: 'relative', width: 'calc(100% - 0px)', minWidth: 980}} id="comp-if6tyiycinlineContent" className="strc1inlineContent">
    <div style={{position: 'relative', width: '100%', left: 0, flex: 980, marginLeft: 0, minWidth: 980, top: 0, marginTop: 0, marginBottom: 0, height: '', display: 'flex', bottom: '', right: ''}} data-content-width={980} data-is-mesh="true" className="strc1" id="mediait1obida7">
      <div style={{position: 'absolute', top: 0, width: '100%', height: '100%', overflow: 'hidden', pointerEvents: 'auto', left: 0, right: 0, bottom: 0}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name data-media-type data-use-clip-path data-needs-clipping data-render-type="bolt" className="strc1balata" id="mediait1obida7balata">
        <div style={{position: 'absolute', width: '100%', height: '100%', top: 0, backgroundColor: 'rgba(130, 228, 213, 1)'}} className="bgColor" id="mediait1obida7balatabgcolor">
          <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="mediait1obida7balatabgcoloroverlay" className="bgColoroverlay" />
        </div>
      </div>
      <div style={{width: '100%', position: 'relative', top: 0, bottom: 0}} className="strc1inlineContent" id="mediait1obida7inlineContent">

        <div id="mediait1obida7inlineContent-gridWrapper" data-mesh-internal="true">
          <div id="mediait1obida7inlineContent-gridContainer" data-mesh-internal="true">
            <div data-is-absolute-layout="false" style={{top: '', bottom: '', left: '', right: '', position: ''}} className="appWidget1" id="comp-jxd8l1ki">
              <div className="appWidget1inlineContent" id="comp-jxd8l1kiinlineContent">
                <div id="comp-jxd8l1kiinlineContent-gridWrapper" data-mesh-internal="true">
                  <div id="comp-jxd8l1kiinlineContent-gridContainer" data-mesh-internal="true">
                    <div data-is-responsive="false" style={{top: '', bottom: '', left: '', right: '', position: ''}} className="style-jxd8lq6e" id="comp-jxd8lq60">

                    <form className="style-jxd8lq6eform" id="comp-jxd8lq60form">
                        <div id="comp-jxd8lq60form-gridWrapper" data-mesh-internal="true">
                          <div id="comp-jxd8lq60form-gridContainer" data-mesh-internal="true">
                            <div className="style-jxd8lq6r_left-direction style-jxd8lq6r" data-disabled="false" style={{width: 447, height: 30}} id="comp-jxd8lq6j">
                              <label style={{paddingLeft: 0, paddingRight: 20, display: 'none', marginBottom: 9, textAlign: 'left', direction: 'ltr'}} htmlFor="comp-jxd8lq6jinput" id="comp-jxd8lq6jlabel" className="style-jxd8lq6rlabel" />
                                <div id="comp-jxd8lq6jinput-wrapper" className="style-jxd8lq6rinput-wrapper">
                                  <div id="comp-jxd8lq6jprefix" className="style-jxd8lq6rprefix" />
                                    <input type="text" style={{paddingLeft: 12}} defaultValue="Name" maxLength={100} className="has-custom-focus style-jxd8lq6rinput" id="comp-jxd8lq6jinput" />
                                  </div>
                                  <p style={{display: 'none'}} id="comp-jxd8lq6jmessage" className="style-jxd8lq6rmessage" />
                                </div>
                                <div className="style-jxd8lq6z2_left-direction style-jxd8lq6z2" data-disabled="false" style={{width: 447, height: 30}} id="comp-jxd8lq6t">
                                  <label style={{paddingLeft: 0, paddingRight: 20, display: 'none', marginBottom: 9, textAlign: 'left', direction: 'ltr'}} htmlFor="comp-jxd8lq6tinput" id="comp-jxd8lq6tlabel" className="style-jxd8lq6z2label" />
                                  <div id="comp-jxd8lq6tinput-wrapper" className="style-jxd8lq6z2input-wrapper">
                                    <div id="comp-jxd8lq6tprefix" className="style-jxd8lq6z2prefix" />
                                    <input type="email" style={{paddingLeft: 12}} defaultValue="Email" maxLength={250} className="has-custom-focus style-jxd8lq6z2input" id="comp-jxd8lq6tinput" />
                                  </div>
                                  <p style={{display: 'none'}} id="comp-jxd8lq6tmessage" className="style-jxd8lq6z2message" />
                                </div>
                                <div className="style-jxd8lq7a1_left-direction style-jxd8lq7a1" style={{width: 447, height: 126}} data-state="valid" id="comp-jxd8lq74">
                                    <label style={{paddingLeft: 0, paddingRight: 20, display: 'none', marginBottom: 9, textAlign: 'left', direction: 'ltr'}} htmlFor="comp-jxd8lq74textarea" id="comp-jxd8lq74label" className="style-jxd8lq7a1label" />
                                    <textarea style={{paddingLeft: 12, paddingRight: 10}} placeholder="Type your message here..." required className="has-custom-focus style-jxd8lq7a1textarea" id="comp-jxd8lq74textarea" defaultValue={""} />
                                </div>
                                <div id="comp-jxd8lq7d" data-align="right" data-disabled="false" data-margin={0} data-should-use-flex="true" data-width={85} data-height={25} style={{cursor: 'pointer', height: 25, minHeight: 22, width: 85}} className="style-jxd8lq7l" data-state="desktop shouldUseFlex right">
                                  <button style={{cursor: 'pointer'}} id="comp-jxd8lq7dlink" className="style-jxd8lq7llink">
                                    <span style={{marginRight: 0}} id="comp-jxd8lq7dlabel" className="style-jxd8lq7llabel">Submit</span>
                                  </button>
                                </div>
                                <div data-packed="true" data-vertical-text="false" style={{width: 405, pointerEvents: 'none', visibility: 'hidden'}} data-hidden="true" className="txtNew" id="comp-jxd8lq7o">
                                  <p className="font_8" style={{textAlign: 'right'}}>
                                    <span className="color_11">Thanks for submitting!</span>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="mediait1obida7inlineContent-wedge-3" data-mesh-internal="true" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  <div data-packed="true" data-vertical-text="false" style={{width: 559, pointerEvents: 'none', visibility: 'inherit', fontSize: '15px'}} className="txtNew" id="iebd057y">
    <p className="font_7" style={{lineHeight: '1.5em', textAlign: 'center'}}>
      <span>U 506, 7 Washington Avenue&nbsp;Riverwood, NSW&nbsp;2210</span>
    </p>
    <p className="font_7" style={{lineHeight: '1.5em', textAlign: 'center'}}>
      <span>Tel: 123-456-7890</span>
    </p>
    <p className="font_7" style={{lineHeight: '1.5em', textAlign: 'center'}}>
      <span>Fax: 123-456-7890</span>
    </p>
    <p className="font_7" style={{lineHeight: '1.5em', textAlign: 'center'}}>
      <span>
        <object height={0}>
          <a className="auto-generated-link" data-auto-recognition="true" data-content="info@mysite.com" href="mailto:info@mysite.com" data-type="mail">info@aussiept.com</a>
        </object>
      </span>
    </p>
  </div>
  
  <div data-packed="true" data-vertical-text="false" style={{width: 350, pointerEvents: 'none', visibility: 'inherit'}} className="txtNew" id="iebd057y_1">
    <h2 className="font_2" style={{textAlign: 'center'}}>
      <span style={{letterSpacing: '0.1em', fontSize: '60px'}}>Contact</span>
    </h2>
  </div>
  <div data-is-responsive="false" style={{width: 5, height: 65, visibility: 'inherit'}} className="vl1" id="iebd057y_0">
    <div id="iebd057y_0line" className="vl1line" />
  </div>
    <div tabIndex={-1} role="region" aria-label="Contact" style={{left: 0, marginLeft: 0, width: '100%', minWidth: 'initial', top: '', bottom: '', right: '', height: 20, position: ''}} className="AutoWidthAnchorSkin" id="iecfudcd">&nbsp;
    </div>

<section style={{left: 0, width: '100%', minWidth: 980, height: 'auto', top: '', bottom: '', right: '', position: '', marginLeft: 0}} data-responsive="true" data-is-screen-width="true" data-col-margin={0} data-row-margin={0} className="strc1" id="comp-iexzyuh2">
  <div style={{position: 'absolute', top: 0, width: 'calc(100% - 0px)', height: '100%', overflow: 'hidden', pointerEvents: 'auto', minWidth: 980, left: 0, right: 0, bottom: 0}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name data-media-type data-use-clip-path data-needs-clipping data-render-type="bolt" className="strc1balata" id="comp-iexzyuh2balata">
    <div style={{position: 'absolute', width: '100%', height: '100%', top: 0, backgroundColor: 'transparent'}} className="bgColor" id="comp-iexzyuh2balatabgcolor">
      <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="comp-iexzyuh2balatabgcoloroverlay" className="bgColoroverlay" />
      </div>
    </div>
    <div style={{position: 'relative', width: 'calc(100% - 0px)', minWidth: 980}} id="comp-iexzyuh2inlineContent" className="strc1inlineContent">
      <div style={{position: 'relative', width: '100%', left: 0, flex: 980, marginLeft: 0, minWidth: 980, top: 0, marginTop: 0, marginBottom: 0, height: '', display: 'flex', bottom: '', right: ''}} data-content-width={980} data-is-mesh="true" className="strc1" id="mediait1obidb1">
        <div style={{position: 'absolute', top: 0, width: '100%', height: '100%', overflow: 'hidden', pointerEvents: 'none', left: 0, right: 0, bottom: 0, clip: 'rect(0px, auto, 839px, 0px)'}} data-page-id="c1dmp" data-enable-video="true" data-bg-effect-name="BgParallax" data-media-type="Image" data-use-clip-path data-needs-clipping="true" data-render-type="bolt" className="strc1balata" id="mediait1obidb1balata">
          <div style={{position: 'fixed', width: '100%', height: '100%', top: 0, backgroundColor: 'rgba(255, 255, 255, 1)'}} className="bgColor" id="mediait1obidb1balatabgcolor">
            <div style={{width: '100%', height: '100%', position: 'absolute', top: 0, backgroundImage: '', opacity: 1}} id="mediait1obidb1balatabgcoloroverlay" className="bgColoroverlay" />
          </div>
          <wix-bg-media style={{position: 'fixed', pointerEvents: 'none', width: '100%', height: '100%', top: 0, left: 0, willChange: 'transform', transform: 'translate3d(0px, 149px, 0px)'}} data-fitting="fill" data-align="center" data-container-id="mediait1obidb1" data-page-id="c1dmp" data-media-comp-type="image" data-is-full-height="true" data-bg-effect-name="BgParallax" data-container-size="980, 839" id="mediait1obidb1balatamedia" className="bgMedia">
            <wix-image style={{width: '100%', height: '100%', position: 'absolute', top: 0, left: 0}} data-has-bg-scroll-effect="true" data-image-info="{&quot;imageData&quot;:{&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-iexzzgzp&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;c1dmp&quot;,&quot;isPreset&quot;:true,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;4_2880px.jpg&quot;,&quot;uri&quot;:&quot;84770f_8506293a29b44887a8a0cb383c0c2078.jpg&quot;,&quot;description&quot;:&quot;private/allMedia_picture&quot;,&quot;width&quot;:2880,&quot;height&quot;:1672,&quot;alt&quot;:&quot;&quot;,&quot;artist&quot;:{&quot;id&quot;:&quot;&quot;,&quot;name&quot;:&quot;&quot;},&quot;displayMode&quot;:&quot;fill&quot;},&quot;containerId&quot;:&quot;mediait1obidb1&quot;,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;}" data-has-ssr-src data-is-svg="false" data-is-svg-mask="false" data-bg-effect-name="BgParallax" data-type="image" id="mediait1obidb1balatamediaimage" className="bgImage" data-src="https://static.wixstatic.com/media/84770f_8506293a29b44887a8a0cb383c0c2078.jpg/v1/fill/w_980,h_839,al_c,q_85,usm_0.66_1.00_0.01/84770f_8506293a29b44887a8a0cb383c0c2078.webp">
              <img id="mediait1obidb1balatamediaimageimage" style={{width: '100%', height: '100%', objectPosition: '90% 50%', objectFit: 'cover'}} alt data-type="image" itemProp="image" src="https://static.wixstatic.com/media/84770f_8506293a29b44887a8a0cb383c0c2078.jpg/v1/fill/w_980,h_839,al_c,q_85,usm_0.66_1.00_0.01/84770f_8506293a29b44887a8a0cb383c0c2078.webp" />
            </wix-image>
          </wix-bg-media>
        </div>
        <div style={{width: '100%', position: 'relative', top: 0, bottom: 0}} className="strc1inlineContent" id="mediait1obidb1inlineContent">
          <div id="mediait1obidb1inlineContent-gridContainer" data-mesh-internal="true" />
        </div>
      </div>
    </div>
  </section>
              <div id="c1dmpinlineContent-wedge-4" data-mesh-internal="true" />
              <div id="c1dmpinlineContent-wedge-13" data-mesh-internal="true" />
              </div>

</main>

    );
  };
  
  export default PageInfo;