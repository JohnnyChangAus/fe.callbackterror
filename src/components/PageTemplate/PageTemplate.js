import React, { Fragment } from 'react';
import Header from "./components/Header/Header";
import PageMain from "./components/PageMain/PageMain";


class PageTemplate extends React.Component {
    constructor() {
        super();
    };

    render() {
        return (
            <div>
                <Header />
                <PageMain />
            </div>
        )
    }

};

export default PageTemplate;