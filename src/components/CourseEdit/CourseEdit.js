import React from "react";

import "../../config/bootstrap/bootstrap.css";
import "../../config/bootstrap/bootstrap-responsive.css";
import "../../config/bootstrap/bootstrap-overrides.css";
import "../../config/elements.css";
import "../../config/icons.css";
import "../../config/layout.css";
import "../../config/compiled/new-user.css";
// import "../NewStudent/NewStudent.scss";
import * as CourseAPI from "../../api/CourseAPI";



class CourseEdit extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      data: {},
      id: ""
    };
  }

  async componentDidMount() {
    const { id } = this.props.match.params;
    const response = await CourseAPI.courseGet(id);
    console.log(response);
    this.setState({
      data: response,
      isLoading: false,
      id: id,
    });
  }

  handleChange = (e) => {
      this.setState({
        data: {...this.state.data, [e.target.name] : e.target.value}
      })
  }

  handleCancel = () => {
      this.props.history.goBack();
  }

  handleSave = async () => {
    const response = await CourseAPI.courseUpdate(this.state.id, this.state.data)
    console.log(response)
    this.props.history.goBack();
  }

  render() {
    if (this.state.isLoading) {
      return <p>Loading...</p>;
    }
    return (
      <div className="content">
        <div className="container-fluid">
          <div id="pad-wrapper" className="user-list">
            <div className="row-fluid header"></div>
            <div id="pad-wrapper" class="new-user">
              <div class="row-fluid header">
                <h3>Edit this course</h3>
                {/* <p>{JSON.stringify(this.state.data)}</p> */}
              </div>
              <div class="row-fluid form-wrapper">
                <div class="span9 with-sidebar">
                  <div class="container">
                    <form class="new_user_form inline-input">
                      <div class="span12 field-box">
                        <label>Course Name:</label>
                        <input
                          class="span9"
                          type="text"
                          name="course_name"
                          value={this.state.data.course_name}
                          onChange={this.handleChange}
                        />
                      </div>
                      <div class="span12 field-box">
                        <label>Duration:</label>
                        <input
                          class="span9"
                          type="text"
                          name="duration"
                          value={this.state.data.duration}
                          onChange={this.handleChange}
                        />
                      </div>
                      <div class="span12 field-box">
                        <label>Aims:</label>
                        <input
                          class="span9"
                          type="text"
                          name="aims"
                          value={this.state.data.aims}
                          onChange={this.handleChange}
                        />
                      </div>
                      <div class="span12 field-box">
                        <label>Price:</label>
                        <input
                          class="span9"
                          type="text"
                          name="price"
                          value={this.state.data.price}
                          onChange={this.handleChange}
                        />
                      </div>
                      <div class="span12 field-box">
                        <label>Description:</label>
                        <textarea
                          class="span9"
                          type="text"
                          name="description"
                          value={this.state.data.description}
                          onChange={this.handleChange}
                        />
                      </div>
                      <div className="span11 field-box actions">
                        <input
                          type="button"
                          className="btn-glow primary"
                          value="Save"
                          onClick={this.handleSave}
                        />
                        <span>OR</span>
                        <input onClick={this.handleCancel} type="reset" value="Cancel" className="reset" />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CourseEdit;
