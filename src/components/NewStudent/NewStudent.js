import React from "react";
import axios from 'axios';
import "../../config/bootstrap/bootstrap.css";
import "../../config/bootstrap/bootstrap-responsive.css";
import "../../config/bootstrap/bootstrap-overrides.css";
import "../../config/layout.css";
import "../../config/elements.css";
import "../../config/icons.css";
import "./NewStudent.scss";
// import { CreateStudent } from '../../api/StudentAPI'


// const yearConstructor = () => {
//     const years = []
//     for (let i = 1900; i > 2020; i++) {
//         years.push(i)
//     }
//     return years
// }

class NewStudent extends React.Component {
    constructor(props) {
        super(props)
        // this.state = {
        //     prefer_name: '',
        //     first_name: '',
        //     last_name: '',
        //     year_of_birth: '',
        //     mobile: '',
        //     email: '',
        //     gender: '',
        //     height: '',
        //     address: ''
        // }

        this.state = {
			prefer_name: {
				value: "",
				validationFailed:"",
				errorMsg: ""
            },
            active_course: {
				value: "",
				validationFailed:"",
				errorMsg: ""
			},
			first_name: {
				value: "",
				validationFailed:"",
				errorMsg: ""
			},
			last_name: {
				value: "",
				validationFailed:"",
				errorMsg: ""
			},
			year_of_birth: {
				value: "",
				validationFailed:"",
				errorMsg: ""
			},
			mobile: {
				value: "",
				validationFailed:"",
				errorMsg: ""
            },
            email: {
				value: "",
				validationFailed:"",
				errorMsg: ""
            },
            physical_gender: {
				value: "",
				validationFailed:"",
				errorMsg: ""
            },
            height: {
				value: "",
				validationFailed:"",
				errorMsg: ""
            },
            address: {
				value: "",
				validationFailed:"",
				errorMsg: ""
            },
            notes: {
				value: "",
				validationFailed:"",
				errorMsg: ""
            },
    }
    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleOnclick = this.handleOnclick.bind(this);
}

    Validator = ()=>{
        const {prefer_name, active_course, first_name, last_name, year_of_birth, mobile, email, physical_gender, height, address, notes} = this.state;
        const newState = {...this.state};
        const emailcheck = /^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/;
        const firstlettercheck = /^[A-Z]/;
        // const numbercheck = /^[0-9]$/;
        const year_of_birth_check = /^(19[0-9][0-9]|20[01][0-9]|2020)$/; //1900-2020
        const phonecheck = /^[0-9]{10}$/;
        const heightcheck = /([12][0-9]{2}|300)/;

        if (prefer_name.value.trim() === ""){
			newState.prefer_name.errorMsg = "Prefer name cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else if (!firstlettercheck.test(prefer_name.value)){
            newState.prefer_name.errorMsg = "First Letter must be capital";
            newState.prefer_name.validationFailed = "true";
            this.setState(newState);
        }else{
			newState.prefer_name.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
        }

        if (active_course.value === ""){
			newState.active_course.errorMsg = "Active course is not set";
			newState.validationFailed = "true";
			this.setState(newState);
		}else{
			newState.active_course.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
        }

        if (first_name.value.trim() === ""){
			newState.first_name.errorMsg = "First name cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else if (!firstlettercheck.test(first_name.value)){
            newState.first_name.errorMsg = "First Letter must be capital";
            newState.first_name.validationFailed = "true";
            this.setState(newState);
        }else{
			newState.first_name.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
        }

        if (last_name.value.trim() === ""){
			newState.last_name.errorMsg = "Last name cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else if (!firstlettercheck.test(last_name.value)){
            newState.last_name.errorMsg = "First Letter must be capital";
            newState.last_name.validationFailed = "true";
            this.setState(newState);
        }else{
			newState.last_name.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
        }

        if (year_of_birth.value.trim() === ""){
			newState.year_of_birth.errorMsg = "Year of birth cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else if (!year_of_birth_check.test(year_of_birth.value)){
            newState.year_of_birth.errorMsg = "Year of birth must be a number within 1900-2020";
            newState.year_of_birth.validationFailed = "true";
            this.setState(newState);
        }else{
			newState.year_of_birth.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
        }

        if (mobile.value.trim() === ""){
			newState.mobile.errorMsg = "Mobile cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else if (!phonecheck.test(mobile.value)){
            newState.mobile.errorMsg = "Mobile must be a 10 digit number";
            newState.mobile.validationFailed = "true";
            this.setState(newState);
        }else{
			newState.mobile.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
        }

        if (email.value.trim() === ""){
			newState.email.errorMsg = "Email cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else if (!emailcheck.test(email.value.toLowerCase())){
            newState.email.errorMsg = "Email format is wrong";
            newState.email.validationFailed = "true";
            this.setState(newState);
        }else{
			newState.email.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
        }

        if (physical_gender.value === ""){
			newState.physical_gender.errorMsg = "Physical gender is not set";
			newState.validationFailed = "true";
			this.setState(newState);
		}else{
			newState.physical_gender.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
        }

        if (height.value.trim() === ""){
			newState.height.errorMsg = "Height cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else if (!heightcheck.test(height.value)){
            newState.height.errorMsg = "Height must be a number within 100-300.";
            newState.height.validationFailed = "true";
            this.setState(newState);
        }else{
			newState.height.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
        }

        if (address.value.trim() === ""){
			newState.address.errorMsg = "Address cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else{
			newState.address.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
        }

        if (notes.value.trim() === ""){
			newState.notes.errorMsg = "Notes cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else{
			newState.notes.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
        }
    }

	handleOnclick = (e)=>{
        e.preventDefault();
		this.Validator();
		const {prefer_name, active_course, first_name, last_name, year_of_birth, mobile, email, physical_gender, height, address, notes} = this.state;
        if (prefer_name.errorMsg ==="" && 
        active_course.errorMsg ==="" && 
        first_name.errorMsg ==="" &&
        last_name.errorMsg ==="" &&
        year_of_birth.errorMsg ==="" && 
        mobile.errorMsg ==="" &&
        email.errorMsg ==="" &&
        physical_gender.errorMsg ==="" && 
        height.errorMsg ==="" &&
        address.errorMsg ===""&&
        notes.errorMsg ===""){
			this.SubmitData();
		}else{
			alert('validation failed');
			}
	}

    SubmitData = () =>{
		let data;
		const {prefer_name, active_course, first_name, last_name, year_of_birth, mobile, email, physical_gender, height, address, notes} = this.state;
		data = {
            prefer_name: prefer_name.value,
            active_course: active_course.value,
			first_name: first_name.value,
			last_name: last_name.value,
			year_of_birth: year_of_birth.value,
            mobile: mobile.value,
            email: email.value,
            physical_gender: physical_gender.value,
            height: height.value,
            address: address.value,
            notes: notes.value,
		}

		axios
		.post("http://localhost:8000/student", data)
		.then(() => alert('Student created'))
		.then(() => window.location.assign("/"))
		.catch((err) => console.log(err));
	}


	handleOnChange = (newValue,id) =>{
		const newState = {...this.state};
		newState[id].value = newValue;
		this.setState(newState);
    }
    // handleChange = (e) => {
    //     this.setState({ [e.target.name]: e.target.value })
    //     console.log(e.target.value)
    // }

    // handlePeferNameChange = (e) => {
    //     const prefer_name = e.target.value;
    //     this.setState({
    //         prefer_name,
    //     })
    // }

    // handleFirstNameChange = (e) => {
    //     const first_name = e.target.value;
    //     this.setState({
    //         first_name,
    //     })
    // }

    // handleLastNameChange = (e) => {
    //     const last_name = e.target.value;
    //     this.setState({
    //         last_name,
    //     })
    // }

    // handleYearOfBirthChange = (e) => {
    //     const year_of_birth = e.target.value;
    //     this.setState({
    //         year_of_birth,
    //     })
    // }

    // handleMobileChange = (e) => {
    //     const mobile = e.target.value;
    //     this.setState({
    //         mobile,
    //     })
    // }

    // handleMobileChange = (e) => {
    //     const mobile = e.target.value;
    //     this.setState({
    //         mobile,
    //     })
    // }

    // handleEmailChange = (e) => {
    //     const email = e.target.value;
    //     this.setState({
    //         email,
    //     })
    // }

    // handleGenderChange = (e) => {
    //     const gender = e.target.value;
    //     this.setState({
    //         gender
    //     })
    // }

    // handleHeightChange = (e) => {
    //     const height = e.target.value;
    //     this.setState({
    //         height
    //     })
    // }

    // handleAddressChange = (e) => {
    //     const address = e.target.value;
    //     this.setState({
    //         address
    //     })
    // }

    // handleSubmit = (e) => {
    //     e.preventDefault();
    //     const data = {
    //         prefer_name: this.state.prefer_name,
    //         first_name: this.state.first_name,
    //         last_name: this.state.last_name,
    //         year_of_birth: this.state.year_of_birth,
    //         mobile: this.state.mobile,
    //         email: this.state.email,
    //         gender: this.state.gender,
    //         height: this.state.height,
    //         address: this.state.address
    //     };
    //     CreateStudent(data)
    // };

    render() {
        const {prefer_name, active_course, first_name, last_name, year_of_birth, mobile, email, physical_gender, height, address, notes} = this.state;
        return (
            <div className="content">

                <div className="container-fluid">
                    <div id="pad-wrapper" className="new-user">
                        <div className="row-fluid header">
                            <h3>Create a new student</h3>
                        </div>
                        <div className="row-fluid form-wrapper">
                            <div className="span9 with-sidebar">
                                <div className="container">
                                    <form onSubmit={this.handleSubmit} className="new_user_form inline-input" >
                                        <div className="span12 field-box">
                                            <label>Prefer Name:</label>
                                            <input className="span9" type="text" id="prefer_name" onChange={(e)=>this.handleOnChange(e.target.value,e.target.id)} />
                                            <small>{prefer_name.errorMsg}</small>
                                        </div>
                                        <div className="span12 field-box">
                                            <label>Active Course:</label>
                                            <div className="ui-select span5">
                                                <select id="active_course" onChange={(e)=>this.handleOnChange(e.target.value,e.target.id)}>
                                                    <option value="not set" >Please choose your active course</option>
                                                    <option value="CA" >Course A</option>
                                                    <option value="CB" >Course B</option>
                                                    <option value="CC" >Course C</option>
                                                    <option value="CD" >Course D</option>
                                                    <option value="CE" >Course E</option>
                                                    <option value="CF" >Course F</option>
                                                </select>
                                                <small>{active_course.errorMsg}</small>
                                            </div>
                                        </div>
                                        <div className="span12 field-box">
                                            <label>First Name:</label>
                                            <input className="span9" type="text" id="first_name" onChange={(e)=>this.handleOnChange(e.target.value,e.target.id)} />
                                            <small>{first_name.errorMsg}</small>
                                        </div>
                                        <div className="span12 field-box">
                                            <label>Last Name:</label>
                                            <input className="span9" type="text" id="last_name" onChange={(e)=>this.handleOnChange(e.target.value,e.target.id)} />
                                            <small>{last_name.errorMsg}</small>
                                        </div>
                                        <div className="span12 field-box">
                                            <label>Year of birth:</label>
                                            <input className="span9" type="text" id="year_of_birth" onChange={(e)=>this.handleOnChange(e.target.value,e.target.id)} />
                                            <small>{year_of_birth.errorMsg}</small>
                                            {/* <div className="ui-select span5" onChange={this.handleYearOfBirthChange}>
                                                <select name="birth-year">
                                                    {years.map((year) => <option value={year}>{year}</option>)}
                                                </select>
                                            </div> */}
                                        </div>
                                        <div className="span12 field-box">
                                            <label>Mobile:</label>
                                            <input className="span9" type="text" id="mobile" onChange={(e)=>this.handleOnChange(e.target.value,e.target.id)} />
                                            <small>{mobile.errorMsg}</small>
                                        </div>
                                        <div className="span12 field-box">
                                            <label>Email:</label>
                                            <input className="span9" type="text" id="email" onChange={(e)=>this.handleOnChange(e.target.value,e.target.id)} />
                                            <small>{email.errorMsg}</small>
                                        </div>
                                        <div className="span12 field-box">
                                            <label>Physical Gender:</label>
                                            {/* <input className="span9" type="text" value={this.state.gender} onChange={this.handleGenderChange} /> */}
                                            <div className="ui-select span5">
                                                <select id="physical_gender" onChange={(e)=>this.handleOnChange(e.target.value,e.target.id)}>
                                                    <option value="not set" >Please choose your gender</option>
                                                    <option value="male" >Male</option>
                                                    <option value="female" >Female</option>
                                                </select>
                                                <small>{physical_gender.errorMsg}</small>
                                            </div>
                                        </div>
                                        <div className="span12 field-box">
                                            <label>Height&nbsp;&#40;cm&#41;:</label>
                                            <input className="span9" type="text" id="height" onChange={(e)=>this.handleOnChange(e.target.value,e.target.id)} />
                                            <small>{height.errorMsg}</small>
                                        </div>
                                        <div className="span12 field-box">
                                            <label>Address:</label>
                                            <input className="span9" type="text" id="address" onChange={(e)=>this.handleOnChange(e.target.value,e.target.id)} />
                                            <small>{address.errorMsg}</small>
                                            {/* <div className="address-fields">
                                                <input className="span12" type="text" placeholder="Street address" />
                                                <input className="span12 small" type="text" placeholder="City" />
                                                <input className="span12 small" type="text" placeholder="State" />
                                                <input className="span12 small last" type="text" placeholder="Postal Code" />
                                            </div> */}
                                        </div>
                                        <div className="span12 field-box textarea">
                                            <label>Notes:</label>
                                            <textarea className="span9" id="notes" onChange={(e)=>this.handleOnChange(e.target.value,e.target.id)}></textarea>
                                            <span className="charactersleft">90 characters remaining. Field limited to 100 characters</span>
                                            <small>{notes.errorMsg}</small>
                                        </div>
                                        <div className="span11 field-box actions">
                                            <button onClick={this.handleOnclick} className="btn-glow primary" value="Create">Create</button>
                                            <span>OR</span>
                                            <input type="reset" value="Cancel" className="reset" />
                                        </div>
                                        <div style={{ display: "" }} className="dev">dev use {JSON.stringify(this.state)}</div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    };
}

export default NewStudent;
