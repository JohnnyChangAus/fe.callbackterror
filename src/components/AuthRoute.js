import {getStorage} from "../service/auth"
import {Redirect, Route} from "react-router-dom"
import React, {useState, useEffect} from 'react'
import { validateRoot } from "../api/UserAPI"

const AuthRoute = ({component: Component, ...rest}) => {

    const [status, setStatus] = useState({
        status: 0,
        isLoading: true,
    })

    useEffect(() => {
        async function getStatus () {
            try {
                const res = await validateRoot(getStorage())
                console.log(res.status)
                setStatus({
                    status: res.status,
                    isLoading: false,
                })
            } catch (err) {
                console.log(err.response.status)
                setStatus({
                    status: err.response.status,
                    isLoading: false,
                })
            }
        }
        getStatus()
    }, [])

    if(status.isLoading) {
        return <div>Loading...</div>
    }
    
    return <Route {...rest} render={(props) => {
        if(status.status === 401) {
            return <>
                <Component {...props} />
                </>
                
        } else {
            return <Redirect to="/home" />

        }
    }} />
}

export default AuthRoute