import React from 'react';
import Axios from 'axios'
import './StudentDetails.scss';
import '../../config/bootstrap/bootstrap-overrides.css';
import '../../config/bootstrap/bootstrap-responsive.css';
import '../../config/bootstrap/bootstrap.css';
import '../../config/layout.css';
import '../../config/elements.css';
import '../../config/icons.css';
import '../../config/compiled/user-profile.css';
import UserImage from "../../images/userIcon.png";
import {DelStudent} from '../../api/StudentAPI';
import {ShowOneStudent} from '../../api/StudentAPI';

// const api = 'http://localhost:8000/student/5fae28d5761b5229346d0d62'

class StudentDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            date: {},
            age: 0,
            id: props.match.params.id,
            path: '/studentedit/'+props.match.params.id
        }
        console.log(this.props.match.params);
        console.log(this.state.id);
    }

    async componentDidMount() {
        const id = this.props.match.params.id
        console.log(id)
        const response = await ShowOneStudent(id)
        // const response = await ShowOneStudent(this.state.id)
        const { _id } = response
        const age = new Date().getFullYear()
        this.setState({
            data: response.data,
            isLoading: false,
            age: age,
            // id: response.data._id
        })
    }

    handleOnClickDel = (e) => {
        const id = this.state.data._id;
        const name = this.state.data.prefer_name
        DelStudent( id, name )
    }

    render() {
        if (this.state.isLoading) {
            return (<p>Loading...</p>)
        }
        return <>
            {/* main container */}
            <div className="content">
                <div className="container-fluid">
                    <div id="pad-wrapper" className="user-profile">
                        {/* <!-- header --> */}
                        <div className="row-fluid header">
                            <div className="span8">
                                <img src={UserImage} className="avatar img-circle user__Img" alt="users' image" />
                                <h3 className="name">{this.state.data.first_name} {this.state.data.last_name}</h3>
                                <span className="area">Attended Course: {this.state.data.course}</span>
                            </div>
                            <button className="btn-flat icon pull-right delete-user" title="Delete Student" onClick={this.handleOnClickDel}>
                                <i className="icon-trash"></i>
                            </button>
                            <a className="btn-flat icon large pull-right edit" href={this.state.path}>
                                Edit this student
                            </a>
                            <a className="btn-flat icon large pull-right edit" href="">
                                InBody Data
                            </a>
                        </div>
                        <div className="row-fluid profile">
                            {/* <!-- bio, new note & orders column --> */}
                            <div className="span9 bio">
                                <div className="profile-box">
                                    {/* <!-- biography --> */}
                                    {/* <div className="span12 section">
                                        <h6>Biography</h6>
                                        <p>There are many variations of passages of Lorem Ipsum available but the majority have
                                        humour suffered alteration in believable some formhumour , by injected humour, or
                                    randomised words which don't look even slightly believable. </p>
                                    </div> */}
                                    <h6>InBody Analysis Data</h6>
                                    <br />
                                    {/* <!-- morris bar & donut charts --> */}
                                    {/* <!-- new comment form --> */}
                                    <div className="span12 section comment">
                                        <h6>Add a quick note</h6>
                                        <p>Add a note about this user to keep a history of your interactions.</p>
                                        <textarea></textarea>
                                        <div className="span12 submit-box pull-right">
                                            <input type="submit" className="btn-glow primary" value="Add Note" />
                                            <span>OR</span>
                                            <input type="reset" value="Cancel" className="reset" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* <!-- side address column --> */}
                            <div className="span3 address pull-right">
                                <h6 className="title-align">Contact Details</h6>
                                <ul>
                                    <li className="ico-li">
                                        <i className="ico-phone-align"></i>
                                        {this.state.data.mobile}
                                    </li>
                                    <li className="ico-li">
                                        <i className="ico-mail-align"></i>
                                        <a href="mailto:{this.state.data.email}">{this.state.data.email}</a>
                                    </li>
                                    <li className="">Height: {this.state.data.height}cm</li>
                                    <li className="">Gender: {this.state.data.gender}</li>
                                    <li className="">Age: {this.state.age - this.state.data.year_of_birth}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <!-- end main container --> */}
        </>
    }
}
export default StudentDetails