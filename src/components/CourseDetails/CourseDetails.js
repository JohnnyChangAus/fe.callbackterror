import React, { useState } from "react";
import Axios from 'axios'

import "../../config/bootstrap/bootstrap.css";
import "../../config/bootstrap/bootstrap-responsive.css";
import "../../config/bootstrap/bootstrap-overrides.css";
import "../../config/elements.css";
import "../../config/icons.css";
import "../../config/layout.css";
import "./CourseDetails.scss";
import { connect } from 'react-redux';
import * as CourseAPI from "../../api/CourseAPI";

const api = 'http://localhost:8000/course/5fae14f0c056bb2c783991d6'

class CourseDetails extends React.Component {
  constructor() {
    super()
    this.state = {
      isLoading: true,
      data: {},
    }
  }

  async componentDidMount() {
    const response = await Axios.get(api);
    console.log(response)
    this.setState({
      data: response.data,
      isLoading: false,
    })
  }

  handleDelete = (e) => {
    e.preventDefault();
    // console.log(e.target.id)
    CourseAPI.courseDelete(this.props.course.id)
    .then(() => alert('Course deleted'))
    .then(() => window.location.assign("/"))
  };

  render() {

    const {course} = this.props;
    const {id, course_name, duration, aims, description, price} = course;
    console.log(course);

    if (this.state.isLoading) {
      return (<p>Loading...</p>)
    }
    return (
      <div className="content">
        <div className="container-fluid">
          <div id="pad-wrapper" className="user-list">
            <div className="row-fluid header">
              <div className="span8">
                <h3 className="name">Course details</h3>
                {/* <span className="area">Graphic Designer</span> */}
              </div>
              <a
                href='/'
                className="btn-flat icon pull-right delete-user"
                data-toggle="tooltip"
                title=""
                data-placement="top"
                data-original-title="Delete user"
                onClick={this.handleDelete}
              >
                Delete
            </a>
              <a href={`/courseedit/${id}`} className="btn-flat icon large pull-right edit">
                Edit this course
            </a>
            </div>
            <div id="pad-wrapper" class="new-user">
              <div class="row-fluid header">
                <h3>{course_name}</h3>
              </div>
              <div class="row-fluid form-wrapper">
                <div class="span9 with-sidebar">
                  <div class="container">
                    <form class="new_user_form inline-input">
                      <div class="span12 field-box">
                        <label>Course Name:</label>
                        <input class="span9" type="text" value={course_name} />
                      </div>
                      <div class="span12 field-box">
                        <label>Duration:</label>
                        <input class="span9" type="text" value={duration} />
                      </div>
                      <div class="span12 field-box">
                        <label>Aims:</label>
                        <input class="span9" type="text" value={aims} />
                      </div>
                      <div class="span12 field-box">
                        <label>Price:</label>
                        <input class="span9" type="text" value={price} />
                      </div>
                      <div class="span12 field-box">
                        <label>Description:</label>
                        <textarea class="span9" type="text" value={description} />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
}

// export default CourseDetails;

// const mapStateToProps = (state) =>({
//   course_name: state.course.course_name,
//   duration: state.course.duration,
//   aims: state.course.aims,
//   description: state.course.description,
//   price: state.course.price,
// })

const mapStateToProps = (state) => ({
  course: state.course,
});

// const mapStateToProps = (state) =>console.log(state)

const CourseDetailsContainer = connect(mapStateToProps)(CourseDetails);

export default CourseDetailsContainer;