import React from "react";
import Header from "../Header/Header"
import Footer from "../Footer/Footer"
import SideBar from "../SideBar/SideBar"
import Content from "../Content/Content"
import "../../config/bootstrap/bootstrap.css";
import "../../config/bootstrap/bootstrap-responsive.css";
import "../../config/bootstrap/bootstrap-overrides.css";
import "../../config/layout.css";
import "../../config/elements.css";
import "../../config/icons.css";
import "./EditStudent.scss";
import { ShowOneStudent, UpdateStudent } from '../../api/StudentAPI'

const yearConstructor = () => {
    const years = []
    for (let i = 1900; i > 2010; i++) {
        years.push(i)
    }
    return years
}

class EditStudent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            data: {},
            id: '',
            path: '/studentdetails/'+this.props.match.params.id
            // prefer_name: '',
            // first_name: '',
            // last_name: '',
            // year_of_birth: '',
            // mobile: '',
            // email: '',
            // gender: '',
            // height: '',
            // address: ''
        }
    }

    handleChange = (e) => {
        this.setState({
            data: { ...this.state.data, [e.target.name]: e.target.value },
        })
    }

    // handlePeferNameChange = (e) => {
    //     const prefer_name = e.target.value;
    //     this.setState({
    //         prefer_name: this.state.data.prefer_name,
    //     })
    // }

    // handleAddressChange = (e) => {
    //     const address = e.target.value;
    //     this.setState({
    //         address,
    //     })
    // }

    handleUpdate = (e) => {
        e.preventDefault();
        const data = this.state.data
        // {
        //     prefer_name: this.state.prefer_name,
        //     first_name: this.state.first_name,
        //     last_name: this.state.last_name,
        //     year_of_birth: this.state.year_of_birth,
        //     mobile: this.state.mobile,
        //     email: this.state.email,
        //     gender: this.state.gender,
        //     height: this.state.height,
        //     address: this.state.address
        // };
        UpdateStudent(this.state.id, this.state.data)
    };

    async componentDidMount() {
        const  id  = this.props.match.params.id
        const response = await ShowOneStudent(id)
        this.setState({
            data: response.data,
            isLoading: false,
            id: id
        })
    }

    render() {
        if (this.state.isLoading) {
            return <div className='container' >Loading...</div>;
        }
        return (
            <>
                <div className="content">

                    <div className="container-fluid">
                        <div id="pad-wrapper" className="new-user">
                            <div className="row-fluid header">
                                <h3>Edit a new student</h3>
                            </div>
                            <div className="row-fluid form-wrapper">
                                <div className="span9 with-sidebar">
                                    <div className="container">
                                        <form className="new_user_form inline-input" onSubmit={this.handleUpdate} >
                                            <div className="span12 field-box">
                                                <label>Prefer Name:</label>
                                                <input className="span9" type="text" name='prefer_name' value={this.state.data.prefer_name} onChange={this.handleChange} />
                                            </div>
                                            <div className="span12 field-box">
                                                <label>Active Course:</label>
                                                <div className="ui-select span5">
                                                    <select>
                                                        <option value="CA" >Lost Weight</option>
                                                        <option value="CB" >Course B</option>
                                                        <option value="CC" >Course C</option>
                                                        <option value="CD" >Course D</option>
                                                        <option value="CE" >Course E</option>
                                                        <option value="CF" >Course F</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="span12 field-box">
                                                <label>First Name:</label>
                                                <input className="span9" type="text" name='first_name' value={this.state.data.first_name} onChange={this.handleChange} />
                                            </div>
                                            <div className="span12 field-box">
                                                <label>Last Name:</label>
                                                <input className="span9" type="text" name='last_name' value={this.state.data.last_name} onChange={this.handleChange} />
                                            </div>
                                            <div className="span12 field-box">
                                                <label>Year of birth</label>
                                                <div className="ui-select span5">
                                                    <select name="year_of_birth" value={this.state.data.year_of_birth}>
                                                        {/* <div className="ui-select span5" onChange={this.handleYearOfBirthChange}>                                              <select name="birth-year">
                                                                {years.map((year) => <option value={year}>{year}</option>)}
                                                            </select>
                                                            </div> */}
                                                        <option value="2018">2018</option>
                                                        <option value="2017">2017</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2015">2015</option>
                                                        <option value="2014">2014</option>
                                                        <option value="2013">2013</option>
                                                        <option value="2012">2012</option>
                                                        <option value="2011">2011</option>
                                                        <option value="2010">2010</option>
                                                        {/* <div className="ui-select span5" onChange={this.handleYearOfBirthChange}>
                                                        <select name="birth-year">
                                                    {years.map((year) => <option value={year}>{year}</option>)}
                                                </select>
                                            </div> */}
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="span12 field-box">
                                                <label>Mobile:</label>
                                                <input className="span9" type="text" name='mobile' value={this.state.data.mobile} onChange={this.handleChange} />
                                            </div>
                                            <div className="span12 field-box">
                                                <label>Email:</label>
                                                <input className="span9" type="text" name='email' value={this.state.data.email} onChange={this.handleChange} />
                                            </div>
                                            <div className="span12 field-box">
                                                <label>Physical Gender:</label>
                                                <div className="ui-select span5">
                                                    <select name='gender' onChange={this.handleChange}>
                                                        <option value="male" >Male</option>
                                                        <option value="female" >Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="span12 field-box">
                                                <label>Heigit (cm)</label>
                                                <input className="span9" type="text" name='height' value={this.state.data.height} onChange={this.handleChange} />
                                            </div>
                                            <div className="span12 field-box">
                                                <label>Address:</label>
                                                <div className="address-fields">
                                                    <input className="span12" type="text" placeholder="Address" name='address' value={this.state.data.address} onChange={this.handleChange} />
                                                    {/* <input className="span12 small" type="text" placeholder="City" value="Brisbane" />
                                                    <input className="span12 small" type="text" placeholder="State" value="QLD" />
                                                    <input className="span12 small last" type="text" placeholder="Postal Code" value="4000" /> */}
                                                </div>
                                            </div>
                                            <div className="span12 field-box textarea">
                                                <label>Notes:</label>
                                                <textarea className="span9" name='note'></textarea>
                                                <span className="charactersleft">90 characters remaining. Field limited to 100 characters</span>
                                            </div>
                                            <div className="span11 field-box actions">
                                                <input type="submit" className="btn-glow primary" value='SAVE' />
                                                <span>OR</span>
                                                <a value="Cancel" className="reset" href={this.state.path}>Cancel</a>
                                            </div>
                                            <div style={{ display: "" }} className="dev">dev use {JSON.stringify(this.state.address)}</div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </>
        );
    };
}

export default EditStudent;
