import axios from "axios"

export const allCourse = async (page, pageSize, keyword) => {
    const response = await axios.get(`http://localhost:8000/courses/${page}/${pageSize}?keyword=${keyword}`)
    console.log(response.status)
    if (response.status >= 200 && response.status < 300) {
        return response.data;
      } else { 
        return response
      }
}

export const courseDelete = async (id) => {
  const response = await axios.delete(`http://localhost:8000/course/${id}`)
  console.log(response.status)
  if (response.status >= 200 && response.status < 300) {
      return response.data;
    } else { 
      return response
    }
}

export const courseGet= async (id) => {
  const response = await axios.get(`http://localhost:8000/course/${id}`)
  console.log(response.status)
  if (response.status >= 200 && response.status < 300) {
      return response.data;
    } else { 
      return response
    }
}

export const courseUpdate = async (id, data) => {
  const response = await axios.put(`http://localhost:8000/course/${id}`, data)
  console.log(response.status)
  if (response.status >= 200 && response.status < 300) {
      return response.data;
    } else { 
      return response
    }
  
}