import Axios from 'axios'

const StudentIndex = "http://localhost:8000/students"
const StudentAPI = "http://localhost:8000/student"

export const AllStudent = async (date) => {
    const res = await Axios.get(StudentIndex)
    return res
}

export const ShowOneStudent = async (id) => {
    const res = await Axios.get(`${StudentAPI}/${id}`)
    return res
}

export const UpdateStudent = async (id, data) => {
    const res = await Axios.patch(`${StudentAPI}/${id}`, data)
        .then(() => alert(`Successfully updated`))

    return res
}

export const CreateStudent = async (data) => {
    const { prefer_name } = data
    Axios.post(StudentAPI, data)
        .then(() => alert(`Student "${prefer_name}" created, going back to Student List Page`))
        .then(() => window.location.href = '/studentlist')
        .catch(err => console.log(err));
}

export const DelStudent = async (id, name) => {
    const res = await Axios.delete(`${StudentAPI}/${id}`)
        .then(alert(`Student: "${name}" deleted`))
        .then(() => window.location.href = '/studentlist')
        .catch(err => console.log(err))
}