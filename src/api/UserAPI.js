import axios from "axios"

export const signup = async (data) => {
  const response = await axios.post("http://localhost:8000/user", data)
  console.log(response.status)
  if (response.status >= 200 && response.status < 300) {
    return response.data;
  } else {
    return response
  }

}

export const login = async (data) => {
  const response = await axios.post(`http://localhost:8000/user/login`, data)
  console.log(response.status)
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    return response
  }

}

export const validateRoot = async (header) => {
  const response = await axios.get(`http://localhost:8000/user/validateroot`, {
    headers: {
      "x-access-token": header,
    },
  })

  console.log(response.status)
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    return response
  }

}