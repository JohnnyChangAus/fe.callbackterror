import { combineReducers, createStore } from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import hardSet from 'redux-persist/lib/stateReconciler/hardSet';
import storageSession from 'redux-persist/lib/storage/session'
import course from './course';

const reducer = combineReducers({
  course,
});

const storageConfig = {
  key: 'root', // 必须有的
  storage: storageSession, // 缓存机制，默认为localStorage
  stateReconciler: hardSet, // 查看 'Merge Process' 部分的具体情况
  // whitelist: ['menu','order'] // reducer 里持久化的数据,除此外均为不持久化数据
};


const myPersistReducer = persistReducer(storageConfig, reducer);

const store = createStore(
  myPersistReducer, 
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);


export const persistor = persistStore(store);

export default store