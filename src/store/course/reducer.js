const DEFAULT_COURSE = {
	course_name: 'Body shape programme',
	duration: 'half year',
	aims: 'Body shape, loss weight',
	description: '43214321',
	price: '1990',
}

export default (state = DEFAULT_COURSE, action) =>{
  switch(action.type){
		case 'SET_COURSE':
			return action.course;

	default:
		return state;
	};
};