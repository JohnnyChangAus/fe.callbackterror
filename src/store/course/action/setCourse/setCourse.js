export default (course) => ({
    type: 'SET_COURSE',
    course,
});