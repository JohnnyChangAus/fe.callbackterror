import React from 'react';
import styled from 'styled-components';
import Flex from '../../components/Flex';
import Header from '../../components/Header/Header';
import PageContent from '../../components/PageGenerator/PageGenerator';

const Wrapper = styled(Flex)`
    flex-direction: row;
    max-width: 100%;
`
const Generator = () => {
    return (
        <Wrapper>
            <Header />
            <PageContent />
        </Wrapper>
    );
}

export default Generator