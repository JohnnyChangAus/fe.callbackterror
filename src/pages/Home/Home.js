import React from 'react';
import Header from '../../components/Header/Header';
// import Footer from '../../Components/Footer/Footer';
import SideBar from '../../components/SideBar';
import Content from '../../components/Content';
// import './Home.scss';
import styled from 'styled-components';
import Flex from '../../components/Flex';

const Wrapper = styled(Flex)`
  flex-direction: row;
  max-width: 100%;
`

class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Wrapper>
        <Header />
        <Content />
        {/* <Footer /> */}
      </Wrapper>
    );
  }
}

export default Home;
