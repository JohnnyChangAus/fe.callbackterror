import React from 'react';
import styled from 'styled-components';
import Flex from '../../components/Flex';
import Header from '../../components/Header/Header';
import PageContent from '../../components/NewStudent/NewStudent';

const Wrapper = styled(Flex)`
    flex-direction: row;
    max-width: 100%;
`
const StudentNew = () => {
    return (
        <Wrapper>
            <Header />
            <PageContent />
        </Wrapper>
    );
}

export default StudentNew