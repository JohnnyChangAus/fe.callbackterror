import React from "react";

import "../../config/bootstrap/bootstrap.css";
import "../../config/bootstrap/bootstrap-responsive.css";
import "../../config/bootstrap/bootstrap-overrides.css";
import "../../config/elements.css";
import "../../config/layout.css";
import "../../config/compiled/signup.css";

import Logo from "../../images/logo.png";

import * as UserAPI from "../../api/UserAPI";

const ulStyle = {
  display: "flex",
  justifyContent: "space-around",
  color: "red",
};

class Signup extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      confirmPassword: "",
      // showAlertUsername: true,
      showAlertEmailEmpty: true,
      showAlertEmailInvalid: false,
      showAlertPasswordEmpty: true,
      showAlertConfirmPasswordMismatch: false,
      showAlertErrorMessage: false,
      errorMessage: "",
    };
  }

  // checkUsername = (prevState, currentState) => {
  //   //to prevent infinite loop
  //   if (prevState !== currentState) {
  //     if (currentState.trim()) {
  //       this.setState({ showAlertUsername: false });
  //     } else {
  //       this.setState({ showAlertUsername: true });
  //     }
  //   }
  // };

  checkEmail = (prevState, currentState) => {
    if (prevState !== currentState) {
      const regex = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
      //check empty
      if (currentState.trim()) {
        this.setState({
          showAlertEmailEmpty: false,
        });
      } else {
        this.setState({
          showAlertEmailEmpty: true,
        });
      }
      //check validation
      if (regex.test(currentState) || this.state.email === "") {
        this.setState({
          showAlertEmailInvalid: false,
        });
      } else {
        this.setState({
          showAlertEmailInvalid: true,
        });
      }
      //hide error message when state change
      this.setState({
        showAlertErrorMessage: false,
      })
    }
  };

  checkPassword = (prevState, currentState) => {
    if (prevState !== currentState) {
      if (currentState.trim()) {
        this.setState({ showAlertPasswordEmpty: false });
      } else {
        this.setState({ showAlertPasswordEmpty: true });
      }
    }
  };

  checkConfirmPassword = (password, confirmPassword) => {
    if (confirmPassword !== password) {
      this.setState({ showAlertConfirmPasswordMismatch: true });
      return false;
    } else {
      this.setState({ showAlertConfirmPasswordMismatch: false });
      return true;
    }
  };

  // option 2
  componentDidUpdate = (prevProps, prevState) => {
    //console.log(prevState.username, this.state.username);

    // this.checkUsername(prevState.username, this.state.username);
    //validate Email
    this.checkEmail(prevState.email, this.state.email);
    this.checkPassword(prevState.password, this.state.password);
  };

  //option 1
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    // const values = Array.from(e.target.elements).map(
    //   (element) => element.value
    // );
    const { email, password, confirmPassword } = this.state;
    if(email !== '' && password !== '') {
      if (this.checkConfirmPassword(password, confirmPassword)) {
        console.log(this.state);
        try {
          const res = await UserAPI.signup({ email, password });
          console.log(res);
          const { history } = this.props;
          history.push("/");
        } catch (e) {
          console.log(e.response.data);
          console.log(e.response.status);
          this.setState({
            showAlertErrorMessage: true,
            errorMessage: e.response.data.message,
          });
          return
        }
      }
    } else {
      return
    }

  };

  render() {
    return (
      <div className="">
        <div className="signup-header">
          <a href="/">
            <img className="logo-topleft" src={Logo} alt="Logo" />
          </a>
        </div>
        <div className="row-fluid signup-wrapper">
          <div className="box">
            <form onSubmit={this.handleSubmit} className="content-wrap">
              {/* <div className="dev">dev use {JSON.stringify(this.state)}</div> */}
              <h6 className="diao">Sign Up</h6>
              {/* <input
                className="span12"
                name="username"
                onChange={this.handleChange}
                type="text"
                placeholder="Username"
              />
              {this.state.showAlertUsername && <li style={{color:"red"}}>Username is required </li>} */}
              <input
                className="span12"
                name="email"
                onChange={this.handleChange}
                type="text"
                placeholder="E-mail address"
              />
              <ul style={ulStyle}>
                {this.state.showAlertEmailEmpty && <li>Email is required </li>}
                {this.state.showAlertEmailInvalid && (
                  <li>Email address format is invalid </li>
                )}
                {this.state.showAlertErrorMessage && (
                  <li>{this.state.errorMessage} </li>
                )}
              </ul>

              <input
                className="span12"
                name="password"
                onChange={this.handleChange}
                type="password"
                placeholder="Password"
              />
              <ul style={ulStyle}>
                {this.state.showAlertPasswordEmpty && (
                  <li>Password is required </li>
                )}
              </ul>

              <input
                className="span12"
                type="password"
                name="confirmPassword"
                onChange={this.handleChange}
                placeholder="Confirm Password"
              />

              <ul style={ulStyle}>
                {this.state.showAlertConfirmPasswordMismatch && (
                  <li>Password mismatch </li>
                )}
              </ul>

              <div className="action">
                <button
                  className="btn-glow primary signup"
                  type="submit"
                  href="/"
                >
                  Sign up
                </button>
              </div>
            </form>
          </div>
          <div className="span4 already">
            <p>Already have an account?</p>
            <a href="/">Sign in</a>
          </div>
        </div>
      </div>
    );
  }
}

export default Signup;
