import React from 'react';
import styled from 'styled-components';
import styles from './CourseNew.module.css';
import Header from '../../components/Header/Header';
import axios from 'axios';

const Title = styled.h3`
	font-size: 24.5px;
	margin: 55px 0px;
	font-weight: normal;
	display: block;
`

class CourseNew extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			name: {
				value: "",
				validationFailed:"",
				errorMsg: ""
			},
			duration: {
				value: "",
				validationFailed:"",
				errorMsg: ""
			},
			aims: {
				value: "",
				validationFailed:"",
				errorMsg: ""
			},
			price: {
				value: "",
				validationFailed:"",
				errorMsg: ""
			},
			description: {
				value: "",
				validationFailed:"",
				errorMsg: ""
			}
		}

		this.HandleOnChange = this.HandleOnChange.bind(this);
		this.HandleOnclick = this.HandleOnclick.bind(this);
	}

	Validator = ()=>{
		const {name, duration, aims, price, description} = this.state;
		const newState = {...this.state};

		if (name.value.trim() === ""){
			newState.name.errorMsg = "Name cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else{
			newState.name.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
		}
		
		if (duration.value.trim() === ""){
			newState.duration.errorMsg = "Duration cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else{
			newState.duration.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
		}

		if (aims.value.trim() === ""){
			newState.aims.errorMsg = "Aims cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else{
			newState.aims.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
		}

		if (price.value.trim() === ""){
			newState.price.errorMsg = "Price cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else if(isNaN(price.value.trim())){
			newState.price.errorMsg = "Price must be a number";
			newState.validationFailed = "true";
			this.setState(newState);
		}else{
			newState.price.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
		}

		if (description.value.trim() === ""){
			newState.description.errorMsg = "Description cannot be null";
			newState.validationFailed = "true";
			this.setState(newState);
		}else{
			newState.description.errorMsg = "";
			newState.validationFailed = "";
			this.setState(newState);
		}
	}

	HandleOnclick = (e)=>{
		this.Validator();
		const {name, duration, aims, price, description} = this.state;
		if (name.errorMsg ==="" && duration.errorMsg ==="" && aims.errorMsg ==="" && price.errorMsg ==="" && description.errorMsg ===""){
			this.SubmitData();
		}else{
			alert('validation failed');
			}
	}

	SubmitData = () =>{
		let data;
		const {name, duration, aims, price, description} = this.state;
		data = {
			course_name: name.value,
			duration: duration.value,
			aims: aims.value,
			description: description.value,
			price: price.value,
		}

		axios
		.post("http://localhost:8000/course", data)
		.then(() => alert('Course created'))
		.then(() => this.props.history.goBack())
		.catch((err) => console.log(err));
	}


	HandleOnChange = (newValue,id) =>{
		const newState = {...this.state};
		newState[id].value = newValue;
		this.setState(newState);
	}

	handleCancel = (e) => {
		e.preventDefault();
		this.props.history.goBack();
	}

	render(){
	const {name, duration, aims, description, price} = this.state;
	return (
		<>
		<Header />
		<div className={styles.wrapper}>
			<Title>Create new course page</Title>
			<div className={styles.container}>
				<label className={styles.label} htmlFor= "name">Name:</label>
				<input id= "name" type="text" className={styles.line} onChange={(e)=>this.HandleOnChange(e.target.value,e.target.id)}/>
				<small className={styles.error}>{name.errorMsg}</small>
			</div>
			<div className={styles.container}>
				<label className={styles.label} htmlFor= "duration">Duration:</label>
				<input id= "duration" type="text" className={styles.line} onChange={(e)=>this.HandleOnChange(e.target.value,e.target.id)}/>
				<small className={styles.error}>{duration.errorMsg}</small>
			</div>
			<div className={styles.container}>
				<label className={styles.label} htmlFor= "aims">Aims:</label>
				<input id= "aims" type="text" className={styles.line} onChange={(e)=>this.HandleOnChange(e.target.value,e.target.id)}/>
				<small className={styles.error}>{aims.errorMsg}</small>
			</div>
			<div className={styles.container}>
				<label className={styles.label} htmlFor= "price">Price:</label>
				<input id= "price" type="text" className={styles.line} onChange={(e)=>this.HandleOnChange(e.target.value,e.target.id)}/>
				<small className={styles.error}>{price.errorMsg}</small>
			</div>
			<div className={styles.container}>
				<label className={styles.label} htmlFor= "description">Description:</label>
				<textarea id="description" type="text" className={styles.description} onChange={(e)=>this.HandleOnChange(e.target.value,e.target.id)}/>
				<small className={styles.error}>{description.errorMsg}</small>
			</div>
			<div className={styles.submit_box}>
				<button className={styles.submit} onClick={this.HandleOnclick}>Create page</button>
				<span className={styles.or}>or</span>
				<input onClick={this.handleCancel} type="reset" value= "cancel" className="cancel"/>
			</div>
		</div >
		</>
    );
	}
}	

export default CourseNew