import React from 'react';

import '../../config/bootstrap/bootstrap.css';
import '../../config/bootstrap/bootstrap-responsive.css';
import '../../config/bootstrap/bootstrap-overrides.css'
import '../../config/elements.css';
import '../../config/layout.css';
import '../../config/compiled/signin.css';
import './Login.scss';

import Logo_white from '../../images/logo-white.png';
import { setStorage, validate } from '../../service/auth'
import axios from "axios";

class Login extends React.Component {
	constructor() {
		super();
		this.state = {
			email: '',
			password: '',
		}
	}

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	handleSubmit =  async (e) => {
		e.preventDefault();
		const result = await validate(this.state)
		console.log(result)
		if (result) {
			setStorage(result.data.token)
			
			const { history } = this.props
			history.replace('/home')
		} else {
			alert('Wrong email or password')
		}
		// const values = Array.from(e.target.elements).map(element => element.value);
	}

	render() {
		return (
			<div className="login-bg">
				<div className="row-fluid login-wrapper">
					<img className="logo" src={Logo_white} alt="logo" />
					<div className="span4 box">
						<form onSubmit={this.handleSubmit} className="content-wrap">
							<h6>Log in</h6>
							<input
								className="span12"
								type="email"
								name="email"
								id="email"
								placeholder="E-mail address"
								value={this.state.email}
								onChange={this.handleChange} />
							<input
								className="span12"
								type="password"
								name="password"
								id="password"
								placeholder="Your password"
								value={this.state.password}
								onChange={this.handleChange} />
							<a href="#" className="forgot">Forgot password?</a>
							<div className="remember">
								<input id="remember-me" type="checkbox" />
								<label for="remember-me">Remember me</label>
							</div>
							<button className="btn-glow primary login" type="submit">
								Log in
							</button>
						</form>
					</div>
					<p>use [abc@abc.com / 4321] to login</p>
					<div style={{ display: "none" }} className="dev">dev use {JSON.stringify(this.state)}</div>
					<div className="span4 no-account">
						<p>Don't have an account?</p>
						<a href="/signup">Sign up</a>
					</div>
				</div>
			</div>
		);
	}
};

export default Login;

