import React from 'react';
import styled from 'styled-components';
import Flex from '../../components/Flex';
import Header from '../../components/Header/Header';
import PageContent from '../../components/CourseList/CourseList';

const Wrapper = styled(Flex)`
    flex-direction: row;
    max-width: 100%;
`
const CourseList = (props) => {
    return (
        <Wrapper>
            <Header />
            <PageContent {...props}/>
        </Wrapper>
    );
}

export default CourseList