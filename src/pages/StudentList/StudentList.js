import React from 'react';
import styled from 'styled-components';
import Flex from '../../components/Flex';
import Header from '../../components/Header/Header';
import PageContent from '../../components/StudentList/StudentList';

const Wrapper = styled(Flex)`
    flex-direction: row;
    max-width: 100%;
`
const StudentList = () => {
    return (
        <Wrapper>
            <Header />
            <PageContent />
        </Wrapper>
    );
}

export default StudentList