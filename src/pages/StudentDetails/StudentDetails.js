import React from 'react';
import styled from 'styled-components';
import Flex from '../../components/Flex';
import Header from '../../components/Header/Header';
import PageContent from '../../components/StudentDetails/StudentDetails';

const Wrapper = styled(Flex)`
    flex-direction: row;
    max-width: 100%;
`
const StudentDetails = (props) => {
    console.log(props.match.params)
    return (
        <Wrapper>
            <Header />
            <PageContent {...props}/>
        </Wrapper>
    );
}

export default StudentDetails