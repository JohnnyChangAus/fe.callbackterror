import React from 'react';
import styled from 'styled-components';
import Flex from '../../components/Flex';
import Header from '../../components/Header/Header';
import PageContent from '../../components/CourseDetails/CourseDetails';

const Wrapper = styled(Flex)`
    flex-direction: row;
    max-width: 100%;
`
const CourseDetails = () => {
    return (
        <Wrapper>
            <Header />
            <PageContent />
        </Wrapper>
    );
}

export default CourseDetails