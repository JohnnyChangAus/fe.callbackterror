import * as UserAPI from "../api/UserAPI";

const validate = async (data) => {
    
    try {
        const res = await UserAPI.login(data)
        console.log(res)
        if (res.status === 200) {
            return res;
        } else { return false }
    } catch(err) {
        console.log(err)
    }

}

const setStorage = (email) => {
    sessionStorage.setItem('token', email)
}

const clearStorage = () => {
    sessionStorage.clear()
}

const getStorage = () => {
    return sessionStorage.getItem('token')
}

export {
    validate,
    setStorage,
    clearStorage,
    getStorage
}