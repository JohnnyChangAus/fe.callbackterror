export default {
    color:{
      primary: '#1976d2',
      secondary: '#dc004e',
      Error:'#f44336' ,
      Warning:'#ff9800',
      Info:'#2196f3',
      Success:'#4caf50',
      textcolor:'#fff',
      textcolor2:'#9fa8b0',
      border:'#d9d9d9',
      buttoncolor:'#1890ff',
      buttoncolorhover:'#40a9ff',
      buttoncolor2:'#e95027',
      buttoncolorhover2:'#e83e10',
    },
  
    backgroundColor:{
      primary:'#001529',
      homepage:'#eff2f5',
    },
  
    fontSize:{
      xl:'30px',
      lg:'20px',
      md:'14px',
    },
    
    fontWeight:{
      light:'400',
      normal: '500',
      bold: '600',
    },
  
    lineHeight:{
      normal: '1.5rem',
      lg:"30px",
    },
  
    gutter:{
      xl:"36px",
      lg: "24px",
      md: "16px",
      sm: "12px",
      xs:"4px",
    },
  
    borderRadius:{
      sm:"2px",
      lg:"20px",
    },
    
  }