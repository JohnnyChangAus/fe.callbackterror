import React, {useState, useEffect} from 'react';

import './App.scss';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import theme from './theme';
import ProtectedRoute from './components/ProtectedRoute'
import AuthRoute from './components/AuthRoute'
import Home from './pages/Home/Home';
import StudentList from './pages/StudentList/StudentList';
import StudentNew from './pages/StudentNew/StudentNew';
import StudentEdit from './pages/StudentEdit/StudentEdit';
import StudentDetails from './pages/StudentDetails/StudentDetails';
import CourseList from './pages/CourseList/CourseList';
import CourseNew from './pages/CourseNew/CourseNew';
import CourseEdit from './pages/CourseEdit/CourseEdit';
import CourseDetails from './pages/CourseDetails/CourseDetails';
import Generator from './pages/Generator/Generator';
import Settings from './pages/Settings/Settings';
import Login from './pages/Login/Login';
import Signup from './pages/Signup/Signup';
import Logout from './components/Logout'
import StudentDelete from './components/StudentDelete'

// for delelopment purpose
import Dev from './pages/Dev/Dev';
// for delelopment purpose



function App() {

	return (
		<ThemeProvider theme={theme}>
			<Router>
				<Switch>
        		{/* for development purpose */}
					<Route path="/dev" component={Dev} />
        		{/* for development purpose */}
					<AuthRoute exact path="/" component={Login} />
					<Route path="/signup" component={Signup} />
					<ProtectedRoute path="/home" component={Home} />
					<ProtectedRoute path="/logout" component={Logout} />
					<ProtectedRoute path="/studentlist" component={StudentList} />
					<ProtectedRoute path="/studentnew" component={StudentNew} />
					<ProtectedRoute path="/studentedit/:id" component={StudentEdit} />
					<ProtectedRoute path="/studentdetails/:id" component={StudentDetails} />
					<ProtectedRoute path="/studentdelete/" component={StudentDelete} />
					<ProtectedRoute path="/courselist" component={CourseList} />
					<ProtectedRoute path="/coursenew" component={CourseNew} />
					<ProtectedRoute path="/courseedit/:id" component={CourseEdit} />
					<ProtectedRoute path="/coursedetails" component={CourseDetails} />
					<ProtectedRoute path="/generator" component={Generator} />
					<ProtectedRoute path="/settings" component={Settings} />
				</Switch>
			</Router>
		</ThemeProvider>
	);
	}
export default App;