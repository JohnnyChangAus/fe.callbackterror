## Front-End Repository ##
### `27 Nov` update note
    * install 3 packages:
        * redux
        * redux-persist
        * react-redux

### `31 Oct` update note
    * fixed mobile side bar

### `30 Oct` update note
    * remove public/logo192.png as react logo
    * remove logo.svg as react logo
    * commonded default content in app.scss and index.css

### `25 Oct` update note
* created folders under src/pages:
    * CourseDetails
    * CourseEdit
    * CourseList
    * CourseNew
    * StudentDetails
    * StudentEdit
    * StudentList
    * StudentNew
    * Signup
* also created routes for each of them from app.js

### `24 Oct` update note
* new node modules : antd 4.7.2
* new node modules : bootstrap 4.5.3
* new a test dev route, localhost:3000/dev, for developing purpose

### `23 Oct` update note
* fixed image path src/config/compiled/signin.css and src/config/elements.css
* deleted duplicated file src/config/comiled/elements.css
* deleted unneeded files _logo.png and _logo-white.png from src/images
* installed "styled-components": "^5.2.0"

### `22 Oct` update note
* create /src/config and put all css, js, font and lib files
* update the img path in those css files
* update image files in /src/images, including logo
* updete /public favicon.ico and index.html
* remove react demo logo from /public
* run npm install after merged to your working directory

### Rre-installed packages:

(check package.json for details)  

* create-react-app
* react-router
* node-sass
* aoxis
* fontawesome packages

### data structure (folders under /src):
* api - all APIs
* components - componemts, such as buttons, 
* config - general settings
* images - pictures, photos and icons
* pages - all pages here

### Conflict happens
When conflict happens, your pull request won't be accepted by team lead until conflict solved.  


It must be discussed with who you conflicted with, then refer the steps below to fix conflict  


![image](conflict.png)

  

================== below is original React Readme ==================

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
